/*
 * Name:
 *     unittest v1.1.4
 *
 * Synopsis:
 *     #define NOCOLOR
 *     #include "unittest.h"
 *
 *     struct unittest {
 *         char const * description; // description of test
 *         int (*testfn)(void *);    // function to test
 *         void *args;               // argument to testfn
 *     };
 *
 *     int  unittest_run_all_implementation  (struct unittest const *tests, size_t len);
 *
 *     #define  TESTFN  (fnname, argname)
 *
 *     #define  unittest_run_all             (tests)
 *     #define  unittest_run_all_and_verify  (tests)
 *     #define  unittest_run_some            (tests, len)
 *
 * Arguments:
 *     tests:   array of tests to be performed
 *     len:     length of array
 *     fnname:  name of function
 *     argname: name of argument
 *
 * Description:
 *     `unittest_run_all_implementation` runs a suite of tests and
 *     reports how many passed
 *
 *     `unittest_run_all` is a macro that calls
 *     unittest_run_all_implementation with the size of the array
 *
 *     `unittest_run_all_and_verify` is the same as unittest_run_all
 *     except it additionally returns 0 if all tests were passed or 1
 *     otherwise. This is intended to be used as main's return value.
 *
 *     `unittest_run_some` is a macro that calls
 *     unittest_run_all_implementation with the given argument
 *
 *     TESTFN expands to a function prototype matching the type expected
 *     by struct unittest. `TESTFN(test1, str)` will expand to
 *     `static int test1 (void *str)`
 *
 * Return Value:
 *     `unittest_run_all_implementation` returns the number of tests passed
 *
 * Example:
 *     see tunittest.c for an example
 *
 * Other Notes:
 *     If your compiler is warning about ANSI escape sequences, or your
 *     terminal does not support them and the PASSED / FAILED text is
 *     printed surrounded by a bunch of garbled text, define the NOCOLOR
 *     macro before including this header
 */
#ifndef BRADENLIB_UNITTEST_H
#define BRADENLIB_UNITTEST_H 1001004L

#include <stdio.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

#ifdef NOCOLOR
#    define ANSIGREEN ""
#    define ANSIRED ""
#    define ANSIRESET ""
#else
#    define ANSIGREEN "\e[1;32m"
#    define ANSIRED "\e[1;37;41m"
#    define ANSIRESET "\e[0m"
#endif /* NOCOLOR */

#define TESTFN(fnname, argname) \
    static int fnname (void *argname)

#define unittest_run_all(tests) \
    unittest_run_all_implementation(tests, sizeof (tests) / sizeof *(tests))

#define unittest_run_all_and_verify(tests) \
    ( \
      ( unittest_run_all_implementation(tests, sizeof (tests) / sizeof *(tests)) \
        == sizeof (tests) / sizeof *(tests) ) \
        ? (0) \
        : (1) )

#define unittest_run_some(tests, len) \
    unittest_run_all_implementation(tests, len)

struct unittest {
    char const *description;
    int (*testfn)(void *);
    void *args;
};

static INLINE int
unittest_run_all_implementation(struct unittest const *tests, size_t len)
{
    size_t i;
    size_t passed = 0;

    for (i = 0; i < len; ++i) {
        int res;
        struct unittest const *seltest = tests + i;

        printf("[Test %lu of %lu] Description: %s\n", i+1, len, seltest->description);
        res = seltest->testfn(seltest->args);
        printf("[Test %lu of %lu] %s\n", i+1, len, res ? (ANSIGREEN "PASSED" ANSIRESET) : (ANSIRED "FAILED" ANSIRESET));
        passed += res;
    }

    printf("%lu of %lu tests passed\n", passed, len);
    return passed;
}

#undef ANSIGREEN
#undef ANSIRED
#undef ANSIRESET

#endif /* BRADENLIB_UNITTEST_H */
