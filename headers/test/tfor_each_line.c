/* Some filler lines to show up in the program output*/
#include <stdio.h>
#include "for_each_line.h"

void
print_line(char const *line, size_t len)
{
    printf("%.*s\n", (int)len, line);
}

void
test_normal(char *linebuf)
{
    FILE *fp = fopen("tfor_each_line.c", "r");

    for_each_line(linebuf, 4096, fp, print_line);
    fclose(fp);
}

void
test_short(char *linebuf)
{
    FILE *fp = fopen("tfor_each_line.c", "r");

    for_each_line(linebuf, 20, fp, print_line);
    fclose(fp);
}

void
test_greedy(char *linebuf)
{
    FILE *fp = fopen("tfor_each_line.c", "r");

    for_each_line_greedy(linebuf, 20, fp, print_line);
    fclose(fp);
}

void
test_until_eof(char *linebuf)
{
    FILE *fp = fopen("tfor_each_line.c", "r");

    while(for_each_line(linebuf, 4096, fp, print_line))
        ;

    fclose(fp);
}

int
main(void)
{
    char linebuf[4096];

    puts("Until EOF:");
    test_until_eof(linebuf);
    puts("Normal:");
    test_normal(linebuf);
    puts("Short (max 20 col):");
    test_short(linebuf);
    puts("Short + Greedy:");
    test_greedy(linebuf);
}
