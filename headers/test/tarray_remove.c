#include <stdio.h>

#include "unittest.h"
#include "array_remove.h"

static void
print_array(int const *array, size_t len)
{
    for (size_t i = 0; i < len; ++i)
        printf("%u, ", array[i]);

    putchar('\n');
}

TESTFN(test_array_remove_2, vfn) {
    void (*pfn)(void *, size_t, size_t, size_t) = vfn;
    int array[] = {1, 2};

    print_array(array, 2);
    pfn(array, 2, sizeof *array, 0);
    print_array(array, 2);

    return array[0] == 2;
}

TESTFN(test_array_remove_5, vfn) {
    void (*pfn)(void *, size_t, size_t, size_t) = vfn;
    int array[] = {1, 2, 3, 4, 5};
    int expect2 = 4;

    print_array(array, 5);
    pfn(array, 5, sizeof *array, 2);
    print_array(array, 5);

    if (vfn == array_removez_fast || vfn == array_remove_fast)
        expect2 = 5;

    return array[1] == 2 && array[2] == expect2;
}

int
main(void)
{
    struct unittest tests[] = {
        {"test array_remove (2 element)",       test_array_remove_2, array_remove},
        {"test array_remove (5 element)",       test_array_remove_5, array_remove},
        {"test array_removez (2 element)",      test_array_remove_2, array_removez},
        {"test array_removez (5 element)",      test_array_remove_5, array_removez},
        {"test array_remove_fast (2 element)",  test_array_remove_2, array_remove_fast},
        {"test array_remove_fast (5 element)",  test_array_remove_5, array_remove_fast},
        {"test array_removez_fast (2 element)", test_array_remove_2, array_removez_fast},
        {"test array_removez_fast (5 element)", test_array_remove_5, array_removez_fast},
    };
    return unittest_run_all_and_verify(tests);
}
