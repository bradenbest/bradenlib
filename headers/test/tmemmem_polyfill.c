#include "unittest.h"
#define MEMMEM_POLYFILL_USE_WRAPPER
#include "memmem_polyfill.h"

TESTFN(test_memmem_polyfill, unused) {
    (void)unused;

    return memmem_polyfill("Hello World", 11, "lo W", 4) != NULL &&
           memmem_polyfill("Hello World", 11, "Bye", 3) == NULL;
}

TESTFN(test_memmem, unused) {
    (void)unused;

    return memmem("Hello World", 11, "lo W", 4) != NULL &&
           memmem("Hello World", 11, "Bye", 3) == NULL;
}

int main() {
    struct unittest tests[] = {
        {"test memmem_polyfill", test_memmem_polyfill, NULL},
        {"test memmem", test_memmem, NULL},
    };

    return unittest_run_all_and_verify(tests);
}
