#include "strlen_delim.h"
#include "unittest.h"

TESTFN(test_strlen_delim, unused) {
    (void)unused;
    char const *str = "Hello World";
    size_t res = strlen_delim(str, ' ');

    printf("Result: %lu\n", res);

    return res == 5;
}

TESTFN(test_strlen_delimz, unused) {
    (void)unused;
    char const *str = "Hello World";
    size_t res = strlen_delimz(str, ',');

    printf("Result: %lu\n", res);

    return res == 11;
}

TESTFN(test_strlen_delim_safe, unused) {
    (void)unused;
    char const *str = "Hello World";
    size_t res = strlen_delim_safe(str, 4, ',');

    printf("Result: %lu\n", res);

    return res == 4;
}

int
main(void)
{
    struct unittest tests[] = {
        {"test strlen_delim(\"Hello World\", 0x20) (expect 5)", test_strlen_delim, NULL},
        {"test strlen_delimz(\"Hello World\", 0x2C) (expect 11)", test_strlen_delimz, NULL},
        {"test strlen_delim_safe(\"Hello World\", 4, 0x2C) (expect 4)", test_strlen_delim_safe, NULL},
    };

    return unittest_run_all_and_verify(tests);
}
