#include <stdio.h>

#include "memeq.h"
#include "unittest.h"

TESTFN(test_memeq, unused)
{
    char const *str = "Hello";
    return memeq("asdfg", 3, "asd", 3) &&
        memeq(str, 0, str, 0) &&
        !memeq("asdfg", 5, "asd", 3) &&
        !memeq("Hello", 5, "Henlo", 5);
}

int
main(void)
{
    struct unittest tests[] = {
        { "test memeq", test_memeq, NULL },
    };

    return unittest_run_all_and_verify(tests);
}
