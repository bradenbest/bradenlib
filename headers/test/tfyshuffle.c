#include <stdio.h>
#include <stdlib.h>

#include "fyshuffle.h"
#include "unittest.h"

static inline void
print_array(int const *array, size_t len)
{
    for (size_t i = 0; i < len; ++i)
        printf("%u ", array[i]);

    putchar('\n');
}

TESTFN(test_fyshuffle, unused) {
    (void)unused;
    int array[] = {1,2,3,4,5,6,7,8,9,10};

    print_array(array, 10);
    fyshuffle(array, 10, sizeof *array);
    print_array(array, 10);
    puts("This requires human verification. Does the list look randomly sorted?");
    return 1;
}

TESTFN(test_fyshuffle_sub, unused) {
    (void)unused;
    int array[] = {1,2,3,4,5,6,7,8,9,10};
    int last_elems[3];

    puts("Shuffling four times and making sure that array[9] doesn't move.");
    print_array(array, 10);
    fyshuffle(array, 9, sizeof *array);
    last_elems[0] = array[9];
    print_array(array, 10);
    fyshuffle(array, 9, sizeof *array);
    last_elems[1] = array[9];
    print_array(array, 10);
    fyshuffle(array, 9, sizeof *array);
    last_elems[2] = array[9];
    print_array(array, 10);
    fyshuffle(array, 9, sizeof *array);
    print_array(array, 10);

    return
        last_elems[0] == 10 &&
        last_elems[1] == 10 &&
        last_elems[2] == 10 &&
        array[9] == 10;
}

TESTFN(test_fyshuffle_large, unused){
    struct {
        long a;
        long b;
        long c;
    } array[] = {
        {0, 0, 0},
        {1, 1, 1},
        {2, 2, 2},
        {3, 3, 3},
        {4, 4, 4},
    };

    if (sizeof *array < sizeof (void *)) {
        printf("fatal: test cannot verify because sizeof struct{long x 3} (%lu) is somehow less than the size of a pointer (%lu)\n", sizeof *array, sizeof (void *));
        return 0;
    }

    printf("Before: ");

    for (size_t i = 0; i < sizeof array / sizeof *array; ++i)
        printf("(%li %li %li) ", array[i].a, array[i].b, array[i].c);

    printf("\n");

    fyshuffle(array, sizeof array / sizeof *array, sizeof *array);

    printf("Before: ");

    for (size_t i = 0; i < sizeof array / sizeof *array; ++i)
        printf("(%li %li %li) ", array[i].a, array[i].b, array[i].c);

    printf("\n");
    printf("Test requires human verification. Is the data sane and in tact?\n");
    return 1;
}

TESTFN(test_fyshuffle_weird, unused){
    struct {
        int a;
        int b;
        int c;
    } array[] = {
        {0, 0, 0},
        {1, 1, 1},
        {2, 2, 2},
        {3, 3, 3},
        {4, 4, 4},
    };

    if (sizeof *array < sizeof (void *)) {
        printf("fatal: test cannot verify because sizeof struct{long x 3} (%lu) is somehow less than the size of a pointer (%lu)\n", sizeof *array, sizeof (void *));
        return 0;
    }

    printf("Before: ");

    for (size_t i = 0; i < sizeof array / sizeof *array; ++i)
        printf("(%i %i %i) ", array[i].a, array[i].b, array[i].c);

    printf("\n");

    fyshuffle(array, sizeof array / sizeof *array, sizeof *array);

    printf("Before: ");

    for (size_t i = 0; i < sizeof array / sizeof *array; ++i)
        printf("(%i %i %i) ", array[i].a, array[i].b, array[i].c);

    printf("\n");
    printf("Test requires human verification. Is the data sane and in tact?\n");
    return 1;
}

TESTFN(test_fyshuffle_oddball_size, unused){
    char string[] = "123456789";

    puts(string);
    fyshuffle(string, 3, 1);;
    puts(string);
    fyshuffle(string, 1, 3);;
    puts(string);
    fyshuffle(string, 9, 1);;
    puts(string);
    fyshuffle(string, 1, 9);;
    puts(string);
    printf("Test requires human verification. Is the data sane and in tact?\n");
    return 1;
}

int
main(void)
{
    struct unittest tests[] = {
        {"Test fyshuffle with int array (always pass)", test_fyshuffle, NULL},
        {"Test fyshuffle with only first 9 members", test_fyshuffle_sub, NULL},
        {"Test fyshuffle with array of objects size=long x 3", test_fyshuffle_large, NULL},
        {"Test fyshuffle with array of objects size=int x 3", test_fyshuffle_weird, NULL},
        {"Test fyshuffle with array of oddball size", test_fyshuffle_oddball_size, NULL},
    };

    return unittest_run_all_and_verify(tests);
}
