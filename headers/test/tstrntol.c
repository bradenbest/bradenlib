#include "strntol.h"
#include "unittest.h"

struct args {
    char const *str;
    size_t len;
    int base;
    long expect;
    long expected_offset;
};

TESTFN(test_strntol, _args) {
    struct args const *args = _args;
    long result = strntol(args->str, args->len, args->base);

    printf("Input: '%.*s' base %u\n", (int)args->len, args->str, args->base);
    printf("Expected: %li\n", args->expect);
    printf("Actual:   %li\n", result);
    return result == args->expect;
}

TESTFN(test_strntole, _args) {
    struct args const *args = _args;
    char *endptr;

    strntole(args->str, args->len, &endptr, args->base);
    printf("Input: '%.*s' base %u\n", (int)args->len, args->str, args->base);
    printf("Expected offset: %lu\n", args->expected_offset);
    printf("Actual offset:   %lu\n", endptr - args->str);
    return endptr - args->str == args->expected_offset;
}

TESTFN(test_overflow, _args) {
    (void)_args;
    long result = strntol("10000000000000", 14, 36); // 36^13 > 2^63

    printf("10000000000000 base 36 = %li\n", result);
    perror("The error received");
    return errno == EOVERFLOW;
}

TESTFN(test_overflow_and_endptr, _args) {
    (void)_args;
    char *endptr;
    char const *nptr = "92233720368547758075";
    long result = strntole(nptr, 20, &endptr, 10); // 36^13 > 2^63

    printf("%s base 10 = %li\n", nptr, result);
    printf("endptr at offset %lu (%s)\n", endptr - nptr, endptr);
    perror("The error received");
    return errno == EOVERFLOW && endptr - nptr == 19;
}

TESTFN(test_base_negative, _args) {
    (void)_args;
    long result = strntol("123", 3, -5);

    printf("123 base -5 = %li\n", result);
    perror("The error received");
    return result == 0 && errno == EINVAL;
}

TESTFN(test_base_one, _args) {
    (void)_args;
    long result = strntol("123", 3, 1);

    printf("123 base 1 = %li\n", result);
    perror("The error received");
    return result == 0 && errno == EINVAL;
}

TESTFN(test_base_37, _args) {
    (void)_args;
    long result = strntol("123", 3, 37);

    printf("123 base 37 = %li\n", result);
    perror("The error received");
    return result == 0 && errno == EINVAL;
}

int
main(void)
{
    struct args args[] = {
        // str,     len, base, expect, expected offset
        {"42",      2,   10,   42,     2},
        {"  42",    4,   10,   42,     4},
        {" +42",    4,   10,   42,     4},
        {" +42abc", 7,   10,   42,     4},
        {" -42abc", 7,   10,   -42,    4},
        {" -0x10",  6,   10,   0,      3},
        {" +0x10",  6,   10,   0,      3},
        {" 0x10",   5,   10,   0,      2},
        {" -0x10",  6,   16,   -16,    6},
        {" +0x10",  6,   16,   16,     6},
        {" 0x10",   5,   16,   16,     5},
        {" -010",   5,   0,    -8,     5},
        {" +010",   5,   0,    8,      5},
        {" 010",    4,   0,    8,      4},
        {" 010",    4,   8,    8,      4},
    };
    struct unittest tests[] = {
        {"Normal test",     test_strntol,             args + 0},
        {"Normal test",     test_strntol,             args + 1},
        {"Normal test",     test_strntol,             args + 2},
        {"Normal test",     test_strntol,             args + 3},
        {"Normal test",     test_strntol,             args + 4},
        {"Normal test",     test_strntol,             args + 5},
        {"Normal test",     test_strntol,             args + 6},
        {"Normal test",     test_strntol,             args + 7},
        {"Normal test",     test_strntol,             args + 8},
        {"Normal test",     test_strntol,             args + 9},
        {"Normal test",     test_strntol,             args + 10},
        {"Normal test",     test_strntol,             args + 11},
        {"Normal test",     test_strntol,             args + 12},
        {"Normal test",     test_strntol,             args + 13},
        {"Normal test",     test_strntol,             args + 14},
        {"endptr test",     test_strntole,            args + 0},
        {"endptr test",     test_strntole,            args + 1},
        {"endptr test",     test_strntole,            args + 2},
        {"endptr test",     test_strntole,            args + 3},
        {"endptr test",     test_strntole,            args + 4},
        {"endptr test",     test_strntole,            args + 5},
        {"endptr test",     test_strntole,            args + 6},
        {"endptr test",     test_strntole,            args + 7},
        {"endptr test",     test_strntole,            args + 8},
        {"endptr test",     test_strntole,            args + 9},
        {"endptr test",     test_strntole,            args + 10},
        {"endptr test",     test_strntole,            args + 11},
        {"endptr test",     test_strntole,            args + 12},
        {"endptr test",     test_strntole,            args + 13},
        {"endptr test",     test_strntole,            args + 14},
        {"Overflow",        test_overflow,            NULL},
        {"Overflow endptr", test_overflow_and_endptr, NULL},
        {"Base -5",         test_base_negative,       NULL},
        {"Base 1",          test_base_one,            NULL},
        {"Base 37",         test_base_37,             NULL},
    };

    return unittest_run_all_and_verify(tests);
}
