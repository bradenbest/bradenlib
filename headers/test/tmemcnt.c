#include <stdio.h>

#include "memcnt.h"
#include "unittest.h"

TESTFN(test_memcnt, va)
{
    return memcnt("asdf123G", 8, MEMCNT_SET_LOWERCASE  MEMCNT_SET_DIGIT) == 1;
}

TESTFN(test_memcntonly, va)
{
    return memcntonly("asdf123", 8, MEMCNT_SET_LOWERCASE  MEMCNT_SET_DIGIT) == 1 &&
        memcntonly("asdf123G", 8, MEMCNT_SET_LOWERCASE  MEMCNT_SET_DIGIT) == 0;
}

int
main(void)
{
    struct unittest tests[] = {
        { "test memcnt", test_memcnt, NULL },
        { "test memcntonly", test_memcntonly, NULL },
    };

    return unittest_run_all_and_verify(tests);
}
