#include "file_get_size.h"
#include "unittest.h"

TESTFN(test_file_get_size, file) {
    FILE *f = file;
    size_t res = file_get_size(f);

    printf("file length: %lu\n", res);
    return res == 32;
}

TESTFN(test_file_get_size_relative, file) {
    FILE *f = file;
    size_t res1 = file_seek_to_delim(f, "\n", 1);
    size_t res2 = file_get_size_relative(f);

    printf("skipped: %lu + 1\n", res1);
    printf("remaining file length: %lu\n", res2);
    return res1 == 11 && res2 == 20;
}

int
main(void)
{
    FILE *f = fopen("tfile_get_size.test", "r");
    struct unittest tests[] = {
        {"test file_get_size", test_file_get_size, f},
        {"test file_get_size_relative", test_file_get_size_relative, f},
    };

    return unittest_run_all_and_verify(tests);
}
