#include "get_line.h"

int
main(void)
{
    char buf[10];
    size_t nread;

    puts("These tests require human verification.");

    puts("Test greedy:");
    puts("Enter 'Hello World!' as the input");
    nread = get_line_greedy(buf, 10);
    printf("Result:   {'%.*s', %lu}\n", (int)nread, buf, nread);
    printf("Expected: {'Hello Worl', 10}\n");
    nread = get_line_greedy(buf, 10);
    printf("Result:   {'%.*s', %lu}\n", (int)nread, buf, nread);
    printf("Expected: {'d!', 2}\n");

    puts("Test non-greedy:");
    puts("Enter 'Hello World!' as the input");
    nread = get_line(buf, 10);
    printf("Result:   {'%.*s', %lu}\n", (int)nread, buf, nread);
    printf("Expected: {'Hello Worl', 10}\n");
    puts("Enter 'Hi' as the input");
    nread = get_line(buf, 10);
    printf("Result:   {'%.*s', %lu}\n", (int)nread, buf, nread);
    printf("Expected: {'Hi', 2}\n");

    puts("Test discard:");
    puts("Enter anything and press enter. The program should ignore your input and exit");
    nread = get_line_discard();
    puts("Done. Was your input ignored?");
}
