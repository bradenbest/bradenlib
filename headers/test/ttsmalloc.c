#include <stdlib.h>
#include <stdio.h>

#define TSMALLOC_ENABLE 1
#include "tsmalloc.h"

int main()
{
    int *foo;

    printf("usage: %lu in use / %lu total\n", tsmalloc_inuse, tsmalloc_total);
    foo = malloc(sizeof *foo);
    printf("call: malloc(%lu) => %p\n", sizeof *foo, foo);
    printf("usage: %lu in use / %lu total\n", tsmalloc_inuse, tsmalloc_total);
    *foo = 34;
    printf("foo = %u\n", *foo);
    free(foo);
    printf("call: free(%p)\n", foo);
    printf("usage: %lu in use / %lu total\n", tsmalloc_inuse, tsmalloc_total);
    printf("to pass, in use must equal total - sizeof *foo\n");

    if (tsmalloc_total - sizeof *foo == tsmalloc_inuse) {
        printf("PASS\n");
        return 0;
    }

    printf("FAIL\n");
    return 1;
}
