#include <string.h>
#include "unittest.h"

TESTFN(testmemcpy1, arg) {
    char const *str = arg;
    char buffer[20];
    size_t len = strlen(str);

    memcpy(buffer, str, len);
    return memcmp(buffer, str, len) == 0;
}

TESTFN(testmemcpy2, arg) {
    char const *str = arg;
    char buffer[20];
    size_t len = strlen(str);

    memcpy(buffer, str, len);
    memcpy(buffer + len, str, len);
    return memcmp(buffer, str, len) == 0;
}

int main(void){
    struct unittest tests[] = {
        {"test memcpy once", testmemcpy1, "Hello"},
        {"test memcpy twice", testmemcpy2, "Hello"},
    };

    return unittest_run_all_and_verify(tests);
}
