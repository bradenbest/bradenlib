#include "unittest.h"
#include "memspn.h"

TESTFN(test_memspn, unused) {
    return memspn("Hello World!", 12, "Helo") == 5;
}

TESTFN(test_memcspn, unused) {
    return memcspn("Hello World!", 12, "!") == 11;
}

int
main(void)
{
    struct unittest tests[] = {
        {"test memspn `Hello World!` `Helo` (expect 5)", test_memspn, NULL},
        {"test memcspn `Hello World!` `!` (expect 11)", test_memcspn, NULL},
    };

    return unittest_run_all_and_verify(tests);
}
