#include <stdio.h>

#include "ivfind.h"
#include "unittest.h"

static int array[] = {1, 2, 3, 4, 5, -1, 7};

TESTFN(test_ivfind, unused) {
    (void)unused;
    int const *match = ivfind(array, 5, 4);

    return match != NULL && *match == 4;
}

TESTFN(test_ivfind_fail, unused) {
    (void)unused;
    int const *match = ivfind(array, 5, 7);

    return match == NULL;
}

TESTFN(test_ivfind_delim, unused) {
    (void)unused;
    int const *match = ivfind_delim(array, -1, 4);

    return match != NULL && *match == 4;
}

TESTFN(test_ivfind_delim_fail, unused) {
    (void)unused;
    int const *match = ivfind_delim(array, -1, 7);

    return match == NULL;
}

int
main(void)
{
    struct unittest tests[] = {
        {"len = 5 {1 2 3 4 5} -> 4", test_ivfind, NULL},
        {"len = 5 {1 2 3 4 5} -> 7", test_ivfind_fail, NULL},
        {"delim = -1 {1 2 3 4 5 -1} -> 4", test_ivfind_delim, NULL},
        {"delim = -1 {1 2 3 4 5 -1} -> 7", test_ivfind_delim_fail, NULL},
    };

    return unittest_run_all_and_verify(tests);
}
