#include <stdio.h>

#include "sorted_insert.h"
#include "unittest.h"

int
icmp(void const *va, void const *vb)
{
    return *(int *)va - *(int *)vb;
}

TESTFN(test_sorted_insert, unused)
{
    (void) unused;
    int array[] = {1, 2, 3, 5};
    int toinsert = 4;

    sorted_insert(array, sizeof array / sizeof *array, sizeof *array, &toinsert, icmp);

    return array[0] == 1 &&
        array[1] == 2 &&
        array[2] == 3 &&
        array[3] == 4 &&
        array[4] == 5;
}

int
main(void)
{
    struct unittest tests[] = {
        { "test sorted_insert", test_sorted_insert, NULL },
    };

    return unittest_run_all_and_verify(tests);
}
