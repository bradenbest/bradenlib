#include <stdio.h>

#include "memcpyb.h"
#include "unittest.h"

struct args {
    size_t bufsz;
    char const *expect;
};

static char const *strings[] = {
    "Hello",
    " ",
    "World",
    "!"
};
static size_t stringslen = sizeof strings / sizeof *strings;

TESTFN(test_memcpye, _args)
{
    struct args *args = _args;
    size_t bufsz = args->bufsz;
    char const *expect = args->expect;

    char buffer[bufsz];
    char *bufp = buffer;

    for (size_t i = 0; i < stringslen; ++i) {
        size_t len = strlen(strings[i]);
        size_t currentoffset = bufp - buffer;

        if (currentoffset + len >= bufsz)
            break;

        bufp = memcpye(bufp, strings[i], len);
    }

    printf("Result:  '%.*s'\n", (int)(bufp - buffer), buffer);
    return memcmp(buffer, expect, strlen(expect)) == 0;
}

TESTFN(test_memcpyb, _args)
{
    struct args *args = _args;
    size_t bufsz = args->bufsz;
    char const *expect = args->expect;

    char buffer[bufsz];
    size_t offset = 0;

    for (size_t i = 0; i < stringslen; ++i) {
        size_t len = strlen(strings[i]);

        if (offset + len >= bufsz)
            break;

        offset += memcpyb(buffer + offset, strings[i], len);
    }

    printf("Result:  '%.*s'\n", (int)(offset), buffer);
    return memcmp(buffer, expect, strlen(expect)) == 0;
}

int
main(void)
{
    struct args fnargs[] = {
        {4096, "Hello World!"},
        {7,    "Hello "},
    };
    struct unittest tests[] = {
        { "memcpye with large buffer (4096) (expect 'Hello World!')", test_memcpye, fnargs + 0 },
        { "memcpye with small buffer (7)    (expect 'Hello ')",       test_memcpye, fnargs + 1 },
        { "memcpyb with large buffer (4096) (expect 'Hello World!')", test_memcpyb, fnargs + 0 },
        { "memcpyb with small buffer (7)    (expect 'Hello ')",       test_memcpyb, fnargs + 1 },

    };

    return unittest_run_all_and_verify(tests);
}
