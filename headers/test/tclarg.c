#include <stdio.h>

#include "clarg.h"
#include "unittest.h"

int arg_callback_normal(char const *arg);
int arg_callback_stdin(char const *unused);
int arg_callback_unrecognized(char const *arg);
int arg_callback_invalid(char const *arg);
int arg_callback_a(char const *unused);
int arg_callback_b(char const *unused);
int arg_callback_c(char const *unused);
int arg_callback_d(char const *param);
int arg_callback_e(char const *unused);

static char *argv[] = {
    "Program Name",
    "-a",
    "-bc",
    "--anaconda",
    "-d",
    "Sparky",
    "-de",
    "Spike",
    "--dog=Max",
    "normal_argument",
    "-",
    "--",
    "--literal=literal",
    NULL
};

static struct clarg argtable[] = {
    {'a', "anaconda", 0, arg_callback_a},
    {'b', NULL,       0, arg_callback_b},
    {'c', NULL,       0, arg_callback_c},
    {'d', "dog",      1, arg_callback_d},
    {'e', NULL,       0, arg_callback_e},
};

int arg_callback_normal(char const *arg){
    printf("Normal argument: %s\n", arg);
    return 1;
}
int arg_callback_stdin(char const *unused){
    (void) unused;
    printf("stdin argument\n");
    return 1;
}
int arg_callback_unrecognized(char const *arg){
    printf("Unrecognized argument `%s`\n", arg);
    return 0;
}
int arg_callback_invalid(char const *arg){
    printf("Unrecognized argument `%s`\n", arg);
    return 0;
}
int arg_callback_a(char const *unused){
    (void) unused;
    printf("detected [-a --anaconda]\n");
    return 1;
}
int arg_callback_b(char const *unused){
    (void) unused;
    printf("detected [-b]\n");
    return 1;
}
int arg_callback_c(char const *unused){
    (void) unused;
    printf("detected [-c]\n");
    return 1;
}
int arg_callback_d(char const *param){
    printf("detected [-d --dog] with param: %s\n", param);
    return 2;
}
int arg_callback_e(char const *unused){
    (void) unused;
    printf("detected [-e]\n");
    return 1;
}

TESTFN(test_1, unused)
{
    (void) unused;

    clarg_set_callback_normal(arg_callback_normal);
    clarg_set_callback_stdin(arg_callback_stdin);
    clarg_set_callback_unrecognized(arg_callback_unrecognized);
    clarg_set_callback_invalid(arg_callback_invalid);

    return clarg_parse_args_simple(argtable, argv);
}

int
main(void)
{
    struct unittest tests[] = {
        { "test arg parsing", test_1, NULL },
    };

    return unittest_run_all_and_verify(tests);
}
