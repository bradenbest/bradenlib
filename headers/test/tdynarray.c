#include "unittest.h"

#define DYNARRAY_TYPE int
#include "dynarray.h"

struct dynarray test;
int sum;

int
add(int *n)
{
    sum += *n;
    return 1;
}

int
intcmp(void const *va, void const *vb)
{
    return *(int *)va - *(int *)vb;
}

TESTFN(test_push, unused) {
    dynarray_push(&test, 3);
    dynarray_push(&test, 2);
    dynarray_push(&test, 1);

    return test.array != NULL &&
        test.array[0] == 3 &&
        test.array[1] == 2 &&
        test.array[2] == 1;
}

TESTFN(test_for_each, fn) {
    int res;

    sum = 0;
    res = dynarray_for_each(&test, fn);

    return res == 1 &&
        sum == 6;
}

TESTFN(test_get_item, unused) {
    int *res = dynarray_get_item(&test, 2);

    return *res == 1;
}

TESTFN(test_get_size, unused) {
    return dynarray_get_size() == sizeof (int);
}

TESTFN(test_qsort, unused) {
    dynarray_qsort(&test, intcmp);

    return test.array[0] == 1 &&
        test.array[1] == 2 &&
        test.array[2] == 3;
}

TESTFN(test_get_length, unused) {
    return dynarray_get_length(&test) == 3;
}

int
main(void)
{
    struct unittest tests[] = {
        {"test push (3, 2, 1)", test_push,     NULL},
        {"test for_each (add)", test_for_each, add},
        {"test get_item",       test_get_item, NULL},
        {"test get_size",       test_get_size, NULL},
        {"test qsort",          test_qsort,    NULL},
        {"test get_length",     test_get_size, NULL},
    };
    return unittest_run_all_and_verify(tests);
}
