/* Name:
 *     inline-restrict
 *
 * Description:
 *     polyfill for inline and restrict keywords
 *
 * Usage:
 *     copy/paste into header
 */

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif
