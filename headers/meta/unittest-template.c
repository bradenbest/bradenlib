#include <stdio.h>

#include (header)
#include "unittest.h"

struct arg {
    (def)
};

TESTFN(test_1, va)
{
    struct arg *arg = va;
    return 1;
}

int
main(void)
{
    struct unittest tests[] = {
        { "description", test_1, NULL },
    };

    return unittest_run_all_and_verify(tests);
}
