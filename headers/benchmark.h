/* Name:
 *     benchmark v1.0.2
 *
 * Synopsis:
 *     #include "benchmark.h"
 *
 *     void    benchmark_start  (void);
 *     time_t  benchmark_end    (void);
 *
 * Description:
 *     Simple wrapper Functions for benchmarking code with microsecond
 *     precision.
 *
 *     `benchmark_start` starts a benchmark
 *     `benchmark_end` ends the last benchmark started
 *
 * Return Value:
 *     `benchmark_end` returns the time elapsed since the last call to
 *     `benchmark_start`. If called without a previous call to
 *     `benchmark_start`, the return value is the time elapsed since the
 *     start of the program.'s execution.
 *
 * Example:
 *     time_t elapsed;
 *
 *     benchmark_start();
 *     puts("How long does it take to print some text to stdout?");
 *     elapsed = benchmark_end();
 *     printf("%lu us\n", elapsed);
 *
 */
#ifndef BRADENLIB_BENCHMARK_H
#define BRADENLIB_BENCHMARK_H 1000002L

#include <time.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

static INLINE void    benchmark_start  (void);
static INLINE time_t  benchmark_end    (void);

static time_t benchmark_timestamp_start;

static INLINE void
benchmark_start(void)
{
    benchmark_timestamp_start = clock();
}

static INLINE time_t
benchmark_end(void)
{
    return clock() - benchmark_timestamp_start;
}

#endif /* BRADENLIB_BENCHMARK_H */
