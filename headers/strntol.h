/*
 * Name:
 *     strntol v1.1.5
 *
 * Synopsis:
 *     long  strntol                 (char const *str, size_t len, int radix);
 *     long  strntole                (char const *str, size_t len, char **endptr, int radix);
 *     long  strntol_implementation  (char const *str, size_t len, char **endptr, int radix);
 *
 * Arguments:
 *     str:     string to convert
 *     len:     length of string
 *     endptr:  pointer to store address of end in
 *     radix:   the radix base (max 36)
 *
 * Description:
 *     Similar to atoi(3) and strtol(3), except it takes a length
 *     argument. Although it has `str` in its name, \0 is not explicitly
 *     checked for. The string can end in a zero byte and the loop will
 *     terminate on it, but this is merely because \0 is not a valid
 *     digit. There is no requirement that str be null-terminated. If
 *     len is longer than the memory that str owns, the behavior is
 *     undefined.
 *
 *     Specifically, strntol discards leading whitespace (as defined by
 *     isspace) and checks the first non-whitespace character for -/+
 *     and adjusts the sign accordingly, and then parses digits in base
 *     (radix) until the first non-digit character, so "   -42abc"
 *     will parse as -42
 *
 *     There is some other behavior inherited from strtol. Namely, if the
 *     radix is 16, then a 0x can appear after the sign and will be
 *     ignored, and if the base is 0, then the base will be inferred by
 *     the prefix. 0x = 16, 0 = 8, otherwise, base 10 is used.
 *
 *     The other part is the address of the first non-digit is stored in
 *     endptr if it is not NULL, however, the behavior deviates from
 *     strtol. *endptr starts at the address of the first digit, and will
 *     only be updated by the digit processing loop. As such, *endptr
 *     points to the first character after the last valid character.
 *
 *     strtol's endptr has a variety of different and rather useless
 *     behaviors. For example, if no digits are found, then endptr
 *     points to the beginning of the string. in strntol_implementation,
 *     endptr will point to the first character that terminated the loop.
 *     This is much simpler and easier to predict.
 *
 *     It is possible for *endptr to be NULL. This will happen if
 *     strntol_implementation returns before getting to the digit
 *     processing loop. Specifically:
 *
 *     - when radix is <0, 1, or >36
 *     - when a string is passed that is either empty or consists of
 *       only whitespace
 *
 *     In both of these cases, strntol_implementation returns 0.
 *
 *     `strntol` calls strntol_implementation with a NULL endptr
 *
 *     `strntole` calls strntol_implementation with the given endptr
 *
 * Errors:
 *     On overflow, errno is set to EOVERFLOW, endptr points to the
 *     digit that caused overflow, and the value up to the last
 *     non-overflowing digit is returned
 *
 *     If radix has an invalid value (<0, 1 or >36), errno is set to
 *     EINVAL
 *
 * Return Value:
 *     returns the parsed number or 0. For errors, check errno.
 */
#ifndef BRADENLIB_STRNTOL_H
#define BRADENLIB_STRNTOL_H 1001005L

#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

#ifdef SANITY_CHECK_RADIXCHR
#include "sassert.h"
#endif

static INLINE long  strntol                 (char const *str, size_t len, int radix);
static INLINE long  strntole                (char const *str, size_t len, char **endptr, int radix);
static INLINE long  strntol_implementation  (char const *str, size_t len, char **endptr, int radix);

static INLINE long
strntol(char const *str, size_t len, int radix)
{
    return strntol_implementation(str, len, NULL, radix);
}

static INLINE long
strntole(char const *str, size_t len, char **endptr, int radix)
{
    return strntol_implementation(str, len, endptr, radix);
}

static INLINE long
strntol_implementation(char const *str, size_t len, char **endptr, int radix)
{
    static char const *radixchr = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    long total = 0;
    int sign = 1;
    char *invalid;
    size_t i;

#ifdef SANITY_CHECK_RADIXCHR
    for (int i = 0; i < 10; ++i)
        sassert(radixchr[0 + i] == '0' + i);

    for (int i = 0; i < 26; ++i)
        sassert(radixchr[10 + i] == 'A' + i);
#endif

    if (endptr != NULL)
        *endptr = NULL;

    if (radix < 0 || radix == 1 || radix > 36) {
        errno = EINVAL;
        return 0;
    }

    while (len > 0 && isspace(*str)) {
        ++str;
        --len;
    }

    if (len == 0)
        return 0;

    if (*str == '+'){
        ++str;
        --len;
    }

    if (*str == '-') {
        ++str;
        --len;
        sign = -1;
    }

    if (radix == 0) {
        if (len > 2 && memcmp(str, "0x", 2) == 0)
            radix = 16;

        else if (len > 1 && *str == '0')
            radix = 8;

        else
            radix = 10;
    }

    if (radix == 16 && memcmp(str, "0x", 2) == 0) {
        str += 2;
        len -= 2;
    }

    invalid = (char *)str;

    for (i = 0; i < len; ++i) {
        char const *res = memchr(radixchr, toupper(str[i]), radix);
        int overflow = 0;

        if (res == NULL)
            break;

        if (total > LONG_MAX / radix)
            overflow = 1;
        else
            total *= radix;

        if (overflow || total > LONG_MAX - (res - radixchr)) {
            errno = EOVERFLOW;
            break;
        }

        total += (res - radixchr);
        ++invalid;
    }

    if (endptr != NULL)
        *endptr = invalid;

    return total * sign;
}

#endif /* BRADENLIB_STRNTOL_H */
