/*
 * Name:
 *     putsv v1.1
 *
 * Synopsis:
 *     int  putsv                  (char const **lines);
 *     int  fputsv                 (char const **lines, FILE *out);
 *     int  putsv_implementation   (char const **lines, FILE *out, int flags);
 *     int  putsvn                 (char const **lines, size_t length);
 *     int  fputsvn                (char const **lines, size_t length, FILE *out);
 *     int  putsvn_implementation  (char const **lines, size_t length, FILE *out, int flags);
 *
 * Arguments:
 *     lines:  array of strings
 *     length: length of array
 *     out:    file to write to
 *     flags:  bitmask
 *
 * Flags:
 *     PUTSV_FLAG_USE_PUTS:
 *         if set, use puts, else use fputs.
 *         more specifically, this causes newlines to be appended to
 *         each line and ignores the file in favor of stdout
 *
 * Description:
 *     Prints every string in lines on their own line. Useful for
 *     printing a bunch of lines of text without having to call puts a
 *     million times. sv stands for string vector.
 *
 *     `putsv_implementation` prints an array of strings until it hits a
 *     NULL. Internally, it calls puts or fputs depending on the flags
 *     argument.
 *
 *     `putsv(lines)` is equivalent to
 *     `putsv_implementation(lines, stdout, PUTSV_FLAG_USE_PUTS)`
 *     Note that `puts` is called on each line
 *
 *     `fputsv(lines, file)` is equivalent to
 *     `putsv_implementation(lines, file, PUTSV_FLAG_NONE)`
 *     Note that `fputs` is called on each line.
 *
 *     the `n` variants (putsvn, fputsvn, putsvn_implementation) are the
 *     same except they take an additional length argument and call
 *     putsvn_implementation. With these functions, the NULL pointer at
 *     the end of the lines array, if it exists, is ignored.
 *
 * Return Value:
 *     Returns the number of characters written, or EOF on error. This is
 *     done in the name of consistency with puts(3).
 *
 * Example:
 *     void usage(void){
 *         static char const *usage_text[] = {
 *             "program - does a thing",
 *             "usage: program foo bar [options]",
 *             "options:",
 *             "  -h  show this help",
 *             "  -q  quiet output",
 *             NULL
 *         };
 *
 *         putsv(usage_text);
 *         exit(1);
 *     }
 */
#ifndef BRADENLIB_PUTSV_H
#define BRADENLIB_PUTSV_H 1001000L

#include <stdio.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

enum putsv_flags {
    PUTSV_FLAG_NONE     = 0x0,
    PUTSV_FLAG_USE_PUTS = 0x1,
    PUTSV_FLAG_END
};

static INLINE int  putsv                  (char const **lines);
static INLINE int  fputsv                 (char const **lines, FILE *out);
static INLINE int  putsv_implementation   (char const **lines, FILE *out, int flags);
static INLINE int  putsvn                 (char const **lines, size_t length);
static INLINE int  fputsvn                (char const **lines, size_t length, FILE *out);
static INLINE int  putsvn_implementation  (char const **lines, size_t length, FILE *out, int flags);

static INLINE int
putsv(char const **lines)
{
    return putsv_implementation(lines, stdout, PUTSV_FLAG_USE_PUTS);
}

static INLINE int
fputsv(char const **lines, FILE *out)
{
    return putsv_implementation(lines, out, PUTSV_FLAG_NONE);
}

/* 1. out is ignored if PUTSV_FLAG_USE_PUTS is set. This void expression
 *    should prevent compilers from warning about unused variables
 *
 * 2. I usually put these types of statements before the declarations,
 *    but with the C89 compliance update, I had to move it down
 */
static INLINE int
putsv_implementation(char const **lines, FILE *out, int flags)
{
    int i;
    int total = 0;
    int retval;
    (void)out; /* 1, 2 */

    for (i = 0; lines[i] != NULL; ++i) {
        if (flags & PUTSV_FLAG_USE_PUTS)
            retval = puts(lines[i]);
        else
            retval = fputs(lines[i], out);

        if (retval == EOF)
            return EOF;

        total += retval;
    }

    return total;
}

static INLINE int
putsvn(char const **lines, size_t length)
{
    return putsvn_implementation(lines, length, stdout, PUTSV_FLAG_USE_PUTS);
}

static INLINE int
fputsvn(char const **lines, size_t length, FILE *out)
{
    return putsvn_implementation(lines, length, out, PUTSV_FLAG_NONE);
}

/* 1. out is ignored if PUTSV_FLAG_USE_PUTS is set. This void expression
 *    should prevent compilers from warning about unused variables
 */
static INLINE int
putsvn_implementation(char const **lines, size_t length, FILE *out, int flags)
{
    size_t i;
    int total = 0;
    int retval;
    (void)out; /* 1 */

    for (i = 0; i < length; ++i) {
        if (flags & PUTSV_FLAG_USE_PUTS)
            retval = puts(lines[i]);
        else
            retval = fputs(lines[i], out);

        if (retval == EOF)
            return EOF;

        total += retval;
    }

    return total;
}

#endif /* BRADENLIB_PUTSV_H */
