/* Name:
 *     dynarray v1.0 - dynamically resizing array
 *
 * Synopsis:
 *     #define DYNARRAY_TYPE struct mycustomtype
 *     #define DYNARRAY_DECLARE
 *     #define DYNARRAY_START_SIZE 16
 *     #define DYNARRAY_GROWTH(x) ((x) * 2)
 *
 *     #include "dynarray.h"
 *
 *     void             dynarray_free        (struct dynarray *da);
 *     size_t           dynarray_get_length  (struct dynarray *da);
 *     void             dynarray_qsort       (struct dynarray *da, dynarray_compare_fn compare);
 *     size_t           dynarray_get_size    (void);
 *     DYNARRAY_TYPE *  dynarray_get_base    (struct dynarray *da);
 *     DYNARRAY_TYPE *  dynarray_get_item    (struct dynarray *da, int index);
 *     int              dynarray_push        (struct dynarray *da, DYNARRAY_TYPE item);
 *     int              dynarray_grow        (struct dynarray *da);
 *     int              dynarray_for_each    (struct dynarray *da, dynarray_callback_fn callback);
 *
 * Arguments:
 *     da:       dynarray
 *     index:    array index
 *     item:     array element
 *     callback: callback function
 *
 * Macros:
 *     DYNARRAY_TYPE: the base type used for the array (default: int)
 *     DYNARRAY_DECLARE: define this to resolve compiler errors regarding
 *         unknown types (default: undefined)
 *     DYNARRAY_START_SIZE: initial capacity (default: 16)
 *     DYNARRAY_GROWTH(x): growth function (default: 2x)
 *
 * Types:
 *     DYNARRAY_TYPE: base element type (user defined, defaults to char)
 *     dynarray_callback_fn: function of type int (DYNARRAY_TYPE *)
 *     dynarray_compare_fn: function of type
 *         int (void const *, void const *)
 *
 * Description:
 *     `dynarray_free` frees allocated memory and resets the array
 *
 *     `dynarray_get_length` gets the length of the array
 *
 *     `dynarray_qsort` calls qsort(3) on the array
 *
 *     `dynarray_get_size` gets the size of the base type
 *
 *     `dynarray_get_base` is the same as `dynarray_get_item(0)`
 *
 *     `dynarray_get_item` gets a pointer to an element of the array
 *
 *     `dynarray_push` pushes an element to the end of the array
 *
 *     `dynarray_grow` grows the array per DYNARRAY_GROWTH
 *
 *     `dynarray_for_each` calls a user-defined callback on each element
 *     of the array. The callback is of type `dynarray_callback_fn`
 *     (aka `int (DYNARRAY_TYPE *)`). The function should return 1 to
 *     indicate success or 0 to indicate failure. If any callback returns
 *     0, `dynarray_for_each` will abort the loop and return 0.
 *
 * Return Value:
 *     `dynarray_get_length` returns the length of the array
 *
 *     `dynarray_get_size` returns the size of the array's base type
 *
 *     `dynarray_get_base` returns a pointer to the array base (same as
 *     dynarray_get_item(0))
 *
 *     `dynarray_get_item` returns a pointer to an element of the array
 *
 *     `dynarray_push`, `dynarray_grow`, `dynarray_for_each` return 1 or
 *     0 to indicate success or failure
 */

#ifndef BRADENLIB_DYNARRAY
#define BRADENLIB_DYNARRAY 1000000L

#include <stdlib.h>

#ifndef DYNARRAY_TYPE
#    define DYNARRAY_TYPE char
#endif

#ifndef DYNARRAY_START_SIZE
#    define DYNARRAY_START_SIZE 16
#endif

#ifndef DYNARRAY_GROWTH
#    define DYNARRAY_GROWTH(x) \
        ( (x) * 2 )
#endif

#ifdef DYNARRAY_DECLARE
    DYNARRAY_TYPE;
#endif

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

struct dynarray {
    DYNARRAY_TYPE * array;
    size_t          length;
    size_t          capacity;
};

typedef int (*dynarray_callback_fn)(DYNARRAY_TYPE *item);
typedef int (*dynarray_compare_fn)(void const *a, void const *b);

static INLINE void             dynarray_free        (struct dynarray *da);
static INLINE size_t           dynarray_get_length  (struct dynarray *da);
static INLINE void             dynarray_qsort       (struct dynarray *da, dynarray_compare_fn compare);
static INLINE size_t           dynarray_get_size    (void);
static INLINE DYNARRAY_TYPE *  dynarray_get_base    (struct dynarray *da);
static INLINE DYNARRAY_TYPE *  dynarray_get_item    (struct dynarray *da, int index);
static INLINE int              dynarray_push        (struct dynarray *da, DYNARRAY_TYPE item);
static INLINE int              dynarray_grow        (struct dynarray *da);
static INLINE int              dynarray_for_each    (struct dynarray *da, dynarray_callback_fn callback);

static INLINE void
dynarray_free(struct dynarray *da)
{
    free(da->array);
    da->array = NULL;
    da->length = 0;
    da->capacity = 0;
}

static INLINE size_t
dynarray_get_length(struct dynarray *da)
{
    return da->length;
}

static INLINE void
dynarray_qsort(struct dynarray *da, dynarray_compare_fn compare)
{
    qsort(da->array, da->length, sizeof (DYNARRAY_TYPE), compare);
}

static INLINE size_t
dynarray_get_size(void)
{
    return sizeof (DYNARRAY_TYPE);
}

static INLINE DYNARRAY_TYPE *
dynarray_get_base(struct dynarray *da)
{
    return dynarray_get_item(da, 0);
}

static INLINE DYNARRAY_TYPE *
dynarray_get_item(struct dynarray *da, int index)
{
    return da->array + index;
}

static INLINE int
dynarray_push(struct dynarray *da, DYNARRAY_TYPE item)
{
    if (da->length == da->capacity)
        if (!dynarray_grow(da))
            return 0;

    da->array[da->length++] = item;
    return 1;
}

static INLINE int
dynarray_grow(struct dynarray *da)
{
    size_t newcap = (da->capacity == 0) ? DYNARRAY_START_SIZE : DYNARRAY_GROWTH(da->capacity);
    size_t allocsize = sizeof *(da->array) * newcap;
    void *newptr = realloc(da->array, allocsize);

    if (newptr == NULL)
        return 0;

    da->array = newptr;
    da->capacity = newcap;
    return 1;
}

static INLINE int
dynarray_for_each(struct dynarray *da, dynarray_callback_fn callback)
{
    size_t i;

    for (i = 0; i < da->length; ++i)
        if (!callback(da->array + i))
            return 0;

    return 1;
}

#endif /* BRADENLIB_DYNARRAY */
