/* Name:
 *     memspn v1.0 - mem span and complimentary span
 *
 * Synopsis:
 *     size_t  memspn                 (void const *base, size_t len, char const *accept);
 *     size_t  memcspn                (void const *base, size_t len, char const *reject);
 *     size_t  memspn_mem             (void const *base, size_t len, char const *accept, size_t acceptlen);
 *     size_t  memcspn_mem            (void const *base, size_t len, char const *reject, size_t rejectlen);
 *     size_t  memspn_implementation  (void const *base, size_t len, char const *delim, size_t delimlen, int flags);
 *
 * Arguments:
 *     base:   char[] memory
 *     len:    length of base in bytes
 *     accept: string
 *     reject: string
 *
 * Flags:
 *     MEMSPN_FLAG_ACCEPT: if set, denotes memspn semantics
 *
 * Description:
 *     `memspn` returns the length of the first segment of base
 *     consisting entirely of characters in accept.
 *
 *     `memcspn` returns the length of the first segment of base
 *     consisting entirely of characters NOT in accept.
 *
 *     `memspn_mem` is the same as `memspn` but it also takes a length
 *     for accept
 *
 *     `memcspn_mem` is the same as `memcspn` but it also takes a length
 *     for reject
 *
 * Notes:
 *     I realize using mem functions to read memory beyond copying seems
 *     a bit nonsensical, however, the mem equivalent of the str
 *     functions are always more predictable since they ignore null
 *     terminators as a boundary. There is definitely a valid usecase.
 *
 *     There is also strpbrk, but a mem equivalent could be trivially
 *     implemented as such:
 *
 *         ptr + memspn(ptr, len, accept)
 */

#ifndef BRADENLIB_MEMSPN_H
#define BRADENLIB_MEMSPN_H 1000000L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

enum memspn_flag {
    MEMSPN_FLAG_NONE   = 0x0,
    MEMSPN_FLAG_ACCEPT = 0x1,
    MEMSPN_FLAG_END
};

static INLINE size_t  memspn                 (void const *base, size_t len, char const *accept);
static INLINE size_t  memcspn                (void const *base, size_t len, char const *reject);
static INLINE size_t  memspn_mem             (void const *base, size_t len, char const *accept, size_t acceptlen);
static INLINE size_t  memcspn_mem            (void const *base, size_t len, char const *reject, size_t rejectlen);
static INLINE size_t  memspn_implementation  (void const *base, size_t len, char const *delim, size_t delimlen, int flags);

static INLINE size_t
memspn(void const *base, size_t len, char const *accept)
{
    return memspn_mem(base, len, accept, strlen(accept));
}

static INLINE size_t
memcspn(void const *base, size_t len, char const *reject)
{
    return memcspn_mem(base, len, reject, strlen(reject));
}

static INLINE size_t
memspn_mem(void const *base, size_t len, char const *accept, size_t acceptlen)
{
    return memspn_implementation(base, len, accept, acceptlen, MEMSPN_FLAG_ACCEPT);
}

static INLINE size_t
memcspn_mem(void const *base, size_t len, char const *reject, size_t rejectlen)
{
    return memspn_implementation(base, len, reject, rejectlen, MEMSPN_FLAG_NONE);
}

/* 1. memspn (length of segment containing only characters in accept)
 *    should return on the first non-match, thus the condition is
 *    `is_accept && !is_in_delim`
 *
 *    memcspn (length of segment containing only characters NOT in
 *    reject) should return on the first match, thus the condition is
 *    `!is_accept && is_in_delim`
 *
 *    In other words, this is an XOR gate.
 */
static INLINE size_t
memspn_implementation(void const *base, size_t len, char const *delim, size_t delimlen, int flags)
{
    char const *base_chr = base;
    size_t i;
    int is_in_delim;
    int is_accept = (flags & MEMSPN_FLAG_ACCEPT) == MEMSPN_FLAG_ACCEPT;

    for (i = 0; i < len; ++i) {
        is_in_delim = memchr(delim, base_chr[i], delimlen) != NULL;

        if (is_accept ^ is_in_delim) /* 1 */
            break;
    }

    return i;
}

#endif /* BRADENLIB_MEMSPN_H */
