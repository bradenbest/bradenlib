/*
 * Name:
 *     strlen_delim v1.2.2
 *
 * Synopsis:
 *     size_t  strlen_delim                      (char const *str, char delim);
 *     size_t  strlen_delimz                     (char const *str, char delim);
 *     size_t  strlen_delim_safe                 (char const *str, size_t maxlen, char delim);
(      size_t  strlen_delim_implementation       (char const *str, size_t maxlen, char delim, int flags);
 *
 * Arguments:
 *     str:   string
 *     delim: value that marks end of string
 *     flags: bitmask
 *
 * Flags:
 *     STRLEN_DELIM_FLAG_CHECK_ZERO: check for \0 as well as delim
 *     STRLEN_DELIM_FLAG_CHECK_LENGTH: check length against a max length
 *
 * Description:
 *     same as strlen(3) but using a different delimeter than strlen.
 *     strlen_delim(str, '\0') is the same as strlen(str).
 *
 *     The 'z' variants additionally check for a null terminator
 *
 *     The `safe` variant is the same as `z` except it also takes a max
 *     length
 *
 *     The `implementation` variants are how the other functions are
 *     implemented. You should not use them directly because they can and
 *     will change with no notice.
 *
 *     Use when strtok(3) is inappropriate, e.g. in read-only strings.
 *
 * Return Value:
 *     The length of the string not including the delimeter
 *
 * Deprecations:
 *     The fast variants are deprecated as of v1.2. Use v1.1 or earlier
 *     for them. The reason these variants were removed is because they
 *     invoke undefined behavior. GLIBC can get away with it because it's
 *     written by the compiler vendor, but this trips asan in 7 out of 8
 *     cases.
 *
 * Notes:
 *     See also memspn, which does essentially the same thing as
 *     `strlen_delim_safe` except delim is a string per strspn.
 *     strlen_delim_safe("foo,bar", 7, ',') is equivalent to
 *     memcspn("foo,bar", 7, ",")
 */
#ifndef BRADENLIB_STRLEN_DELIM_H
#define BRADENLIB_STRLEN_DELIM_H 1002002L

#include <stdlib.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

enum strlen_delim_flags {
    STRLEN_DELIM_FLAG_NONE = 0x0,
    STRLEN_DELIM_FLAG_CHECK_ZERO = 0x1,
    STRLEN_DELIM_FLAG_CHECK_LENGTH = 0x2,
    STRLEN_DELIM_FLAG_END
};

static INLINE size_t strlen_delim                      (char const *str, char delim);
static INLINE size_t strlen_delimz                     (char const *str, char delim);
static INLINE size_t strlen_delim_safe                 (char const *str, size_t maxlen, char delim);
static INLINE size_t strlen_delim_implementation       (char const *str, size_t maxlen, char delim, int flags);

static INLINE size_t
strlen_delim(char const *str, char delim)
{
    return strlen_delim_implementation(str, 0, delim, STRLEN_DELIM_FLAG_NONE);
}

static INLINE size_t
strlen_delimz(char const *str, char delim)
{
    return strlen_delim_implementation(str, 0, delim, STRLEN_DELIM_FLAG_CHECK_ZERO);
}

static INLINE size_t
strlen_delim_safe(char const *str, size_t maxlen, char delim)
{
    static int const flags = STRLEN_DELIM_FLAG_CHECK_ZERO | STRLEN_DELIM_FLAG_CHECK_LENGTH;

    return strlen_delim_implementation(str, maxlen, delim, flags);
}

static INLINE size_t
strlen_delim_implementation(char const *str, size_t maxlen, char delim, int flags)
{
    size_t length = 0;

    if (0) goto exit_unreachable;

    while (1) {
        if (flags & STRLEN_DELIM_FLAG_CHECK_LENGTH && length >= maxlen)
            return length;

        if (flags & STRLEN_DELIM_FLAG_CHECK_ZERO && str[length] == '\0')
            return length;

        if (str[length] == delim)
            return length;

        ++length;
    }

exit_unreachable:
    return length;
}

#endif /* BRADENLIB_STRLEN_DELIM_H */
