/* Name:
 *     memeq v1.0 - strict mem equality check
 *
 * Synopsis:
 *     int  memeq  (void const *s1, size_t n1, void const *s2, size_t n2);
 *
 * Arguments:
 *     s1: string 1
 *     n1: length of string 1
 *     s2: string 2
 *     n2: length of string 2
 *
 * Description:
 *     `memeq` performs a strict equality check. If the two strings point
 *     to the same address, they are automatically considered equal. If
 *     the lengths are different, they are automatically considered
 *     unequal. Otherwise, they are compared with memcmp.
 *
 * Return Value:
 *     `memeq` returns 1 if the strings are equal and 0 otherwise
 */

#ifndef BRADENLIB_MEMEQ_H
#define BRADENLIB_MEMEQ_H 1000000L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

static INLINE int  memeq  (void const *s1, size_t n1, void const *s2, size_t n2);

static INLINE int
memeq(void const *s1, size_t n1, void const *s2, size_t n2)
{
    if (s1 == s2)
        return 1;

    if (n1 != n2)
        return 0;

    return memcmp(s1, s2, n1) == 0;
}

#endif /* BRADENLIB_MEMEQ_H */
