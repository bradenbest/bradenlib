/*
 * Name:
 *     fyshuffle v1.1.2
 *
 * Synopsis:
 *     void  fyshuffle       (void *base, size_t nmemb, size_t size);
 *     void  fyshuffle_swap  (void * restrict va, void * restrict vb, size_t size);
 *
 * Arguments:
 *     base:  base of memory to be shuffled
 *     nmemb: number of members
 *     size:  size of a member
 *
 * Description:
 *     Performs a Fisher-Yates shuffle using rand(3) on base. The
 *     arguments are arranged after qsort(3) rather than fwrite(3). I
 *     know, the libc functions aren't very consistent in their
 *     arguments.
 *
 *     fyshuffle_swap is a generalized swapping function. It's used here
 *     to get rid of the use of malloc from v1.0.2. If the regions of
 *     memory being swapped overlap, the behavior is undefined.
 */
#ifndef BRADENLIB_FYSHUFFLE_H
#define BRADENLIB_FYSHUFFLE_H 1001002L

#include <string.h>
#include <stdlib.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

static INLINE void  fyshuffle       (void *base, size_t nmemb, size_t size);
static INLINE void  fyshuffle_swap  (void * RESTRICT va, void * RESTRICT vb, size_t size);

static INLINE void
fyshuffle(void *base, size_t nmemb, size_t size)
{
    size_t i;

    for (i = 0; i < nmemb - 1; ++i) {
        size_t randoffset = rand() % (nmemb - i);
        char *a = (char *)base + size * (i + 0);
        char *b = (char *)base + size * (i + randoffset);

        if (a != b)
            fyshuffle_swap(a, b, size);
    }
}

/* 1. size >= chunksize is always true but size may not always be a
 *    multiple of chunksize
 */
#define TEMPBUFSIZE (sizeof (void *))
static INLINE void
fyshuffle_swap(void * RESTRICT va, void * RESTRICT vb, size_t size)
{
    static unsigned char temp[TEMPBUFSIZE];
    size_t chunksize = (size < TEMPBUFSIZE) ? (size) : (TEMPBUFSIZE);
    size_t i;

    chunksize -= (size % chunksize); /* 1 */

    for (i = 0; i < size; i += chunksize) {
        memcpy(temp, (char *)va + i, chunksize);
        memcpy((char *)va + i, (char *)vb + i, chunksize);
        memcpy((char *)vb + i, temp, chunksize);
    }
}
#undef TEMPBUFSIZE

#endif /* BRADENLIB_FYSHUFFLE_H */
