/*
 * Name:
 *     setcmpeq v2.0.2
 *
 * Synopsis:
 *     #define SETCMPEQ (var, setvalue, cmpvalue)
 *     #define SETCMPNE (var, setvalue, cmpvalue)
 *     #define SETCMPGT (var, setvalue, cmpvalue)
 *     #define SETCMPGE (var, setvalue, cmpvalue)
 *     #define SETCMPLT (var, setvalue, cmpvalue)
 *     #define SETCMPLE (var, setvalue, cmpvalue)
 *
 * Arguments:
 *     var:      the variable being set
 *     setvalue: the value to set var to
 *     cmpvalue: the value to compare var against after setting it
 *
 * Description:
 *     the expression ((a = b) == c) is too easy to screw up with all
 *     the parens, so I made a macro for it in an attempt to aid
 *     readability. That's what this does.
 *
 *     `SETCMPEQ` sets and compares ==
 *
 *     `SETCMPNE` sets and compares !=
 *
 *     `SETCMPGT` and `setcmpge` set and compare > and >=
 *
 *     `SETCMPLT` and `setcmple` set and compare < and <=
 *
 * Example:
 *     // before
 *     while ((ch = getchar()) != EOF)
 *
 *     // after
 *     while (SETCMPNE(ch, getchar(), EOF))
 */
#ifndef BRADENLIB_SETCMPEQ_H
#define BRADENLIB_SETCMPEQ_H 2000002L

#define SETCMPEQ (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) == (cmpvalue) )

#define SETCMPNE (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) != (cmpvalue) )

#define SETCMPGT (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) >  (cmpvalue) )

#define SETCMPGE (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) >= (cmpvalue) )

#define SETCMPLT (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) <  (cmpvalue) )

#define SETCMPLE (var, setvalue, cmpvalue) \
    ( (var = (setvalue)) <= (cmpvalue) )

static int setcmpeq_silence_empty_translation_unit_warning;

#endif /* BRADENLIB_SETCMPEQ_H */
