/*
 * Name:
 *     tsmalloc v1.0.2
 *
 * Synopsis:
 *     #define TSMALLOC_ENABLE 1
 *     #include "tsmalloc.h"
 *
 *     static size_t tsmalloc_total
 *     static size_t tsmalloc_inuse
 *
 * Description:
 *     Wrapper for malloc / free that tracks memory usage and can be
 *     audited through the tsmalloc_total and tsmalloc_inuse variables.
 *
 *     To use: just `#define TSMALLOC_ENABLE 1` and include this file.
 *     Calls to malloc and free will be automatically converted
 *
 *     This is inherently slower than just using malloc/free, and uses
 *     more memory (sizeof (size_t) bytes per allocation), so this is
 *     intended for debugging purposes only.
 *
 *     By default, this provides a dummy implementation and lets calls to
 *     malloc/free pass through to the libc implementation. If
 *     TSMALLOC_ENABLE is defined as 1, then it wraps with the tracking
 *     added.
 *
 *     As this is header-only, its tracking is per-module. If you want
 *     global memory tracking, use tgmalloc from modules/
 *
 *     tsmalloc stands for tracking static malloc.
 */
#ifndef BRADENLIB_TSMALLOC_H
#define BRADENLIB_TSMALLOC_H 1000002L

#include <stdlib.h>
#include <stddef.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

#ifndef TSMALLOC_ENABLE
#    define TSMALLOC_ENABLE 0
#endif

#if TSMALLOC_ENABLE

#define malloc(size)  tsmalloc_malloc(size)
#define free(ptr)     tsmalloc_free(ptr)

static size_t tsmalloc_total;
static size_t tsmalloc_inuse;

static INLINE void *  tsmalloc_malloc  (size_t size);
static INLINE void    tsmalloc_free    (void *ptr);

static INLINE void *
tsmalloc_malloc(size_t size)
{
    void *res = (malloc)(sizeof tsmalloc_total + size);
    size_t *sizep = res;

    if (res == NULL)
        return NULL;

    *sizep = size;
    tsmalloc_total += size;
    tsmalloc_inuse += size;
    return (char *)res + sizeof tsmalloc_total;
}

static INLINE void
tsmalloc_free(void *ptr)
{
    size_t *sizep;
    void *realptr;

    if (ptr == NULL)
        return;

    realptr = (char *)ptr - sizeof tsmalloc_total;
    sizep = realptr;
    tsmalloc_inuse -= *sizep;
    (free)(realptr);
}
#else /* TSMALLOC_ENABLE */

#define tsmalloc_total  (0LU)
#define tsmalloc_inuse  (0LU)
#define tsmalloc_malloc malloc
#define tsmalloc_free   free

#endif /* TSMALLOC_ENABLE */

#endif /* BRADENLIB_TSMALLOC_H */
