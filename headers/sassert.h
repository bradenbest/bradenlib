/*
 * Name:
 *     sassert v1.0.2
 *
 * Synopsis:
 *     #define sassert(expr)
 *
 * Arguments:
 *     expr: expression to test
 *
 * Description:
 *     Simple assert macro, to replace stdc assert. Checks for NDEBUG
 *     macro
 */
#ifndef BRADENLIB_SASSERT_H
#define BRADENLIB_SASSERT_H 1000002L

#include <stdio.h>
#include <stdlib.h>

#ifdef NDEBUG
#    define sassert(expr) (void)(expr)
#else
#    define sassert(expr) \
        if (!(expr)) { \
            fprintf(stderr, "%s:%u: Assertion failed: '%s'\n", __FILE__, __LINE__, #expr); \
            abort(); \
        }
#endif /* NDEBUG */

static int sassert_silence_empty_translation_unit_warning;

#endif /* BRADENLIB_SASSERT_H */
