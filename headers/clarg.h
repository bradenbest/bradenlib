/* Name:
 *     clarg v1.0 - commandline arg parsing
 *
 * Synopsis:
 *     #include "clarg.h"
 *
 *     typedef int (*clarg_callback_fn)(char const *param);
 *
 *     struct clarg {
 *         char              shortopt;
 *         char *            longopt;
 *         int               has_param;
 *         clarg_callback_fn callback;
 *     };
 *
 *     #define clarg_parse_args_simple(argtab, argv)
 *
 *     void           clarg_set_callback_normal        (clarg_callback_fn callback);
 *     void           clarg_set_callback_stdin         (clarg_callback_fn callback);
 *     void           clarg_set_callback_unrecognized  (clarg_callback_fn callback);
 *     void           clarg_set_callback_invalid       (clarg_callback_fn callback);
 *     struct clarg * clarg_find_short                 (struct clarg *argtab, size_t argtablen, char key);
 *     struct clarg * clarg_find_long                  (struct clarg *argtab, size_t argtablen, char *key);
 *     int            clarg_parse_longopt              (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
 *     int            clarg_parse_multiopt             (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
 *     int            clarg_parse_shortopt             (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
 *     int            clarg_parse_arg                  (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
 *     int            clarg_parse_args                 (struct clarg *argtab, size_t argtablen, char **argv);
 *
 * Callbacks:
 *     clarg_callback_normal:       regular arguments
 *     clarg_callback_stdin:        `-` argument
 *     clarg_callback_unrecognized: unrecognized opts
 *     clarg_callback_invalid:      invalid opts (typically: param mismatch)
 *
 * Constants:
 *     clarg_callback_dummy_1: dummy callback, returns 1
 *     clarg_callback_dummy_0: dummy callback, returns 0
 *
 * Features:
 *     shortopts (-a)
 *     multiopts (-abc)
 *     longopts (--argument)
 *     opt parameters (-a param, --argument=param, -abc param)
 *     disable arg parsing (--)
 *     use stdin (-) (calls callback set by `clarg_set_callback_stdin`)
 *
 * Arguments:
 *     callback:  callback function pointer
 *     argtab:    pointer to first entry of argument table
 *                (defines opt bindings)
 *     argtablen: length of argtab
 *     key:       search key
 *     arg:       current argument
 *     nextarg:   next argument
 *     argv:      argv, from main()
 *
 * Description:
 *     `clarg_set_callback_normal`, `clarg_set_callback_stdin`,
 *     `clarg_set_callback_unrecognized`, `clarg_set_callback_invalid`
 *     set the respective callback function (see Callbacks)
 *
 *     `clarg_find_short`, `clarg_find_long`
 *     find the first entry in the given arg table that matches the key
 *
 *     `clarg_parse_longopt`
 *     parses a longopt of the form `--longopt` or `--longopt=value`
 *
 *     `clarg_parse_multiopt`
 *     parses a multiopt of the form `-abc` or `-abc value`. Note: only
 *     one opt in a multiopt can take a parameter
 *
 *     `clarg_parse_shortopt`
 *     parses a shortopt of the form `-a` or `-a value`
 *
 *     `clarg_parse_arg`
 *     parses a single argument, deferring to `clarg_parse_longopt`,
 *     `clarg_parse_shortopt`, or the function pointed to by
 *     `clarg_callback_normal`
 *
 *     `clarg_parse_args`
 *     parses all arguments given an argtab and argv
 *
 *     `clarg_parse_args_simple`
 *     same as `clarg_parse_args` but it calculates the array length
 *
 * Return Value:
 *     `clarg_find_short`, `clarg_find_long`
 *     return a pointer into argtab pointing to the matching table
 *     entry, or NULL if no match is found
 *
 *     `clarg_parse_longopt`, `clarg_parse_multiopt`,
 *     `clarg_parse_shortopt`, `clarg_parse_arg`
 *     return the number of commandline arguments parsed (0, 1, or 2)
 *     a return value of zero indicates an error
 *
 *     `clarg_parse_args`
 *     returns 1 on success or 0 on failure. Failure means the usage
 *     help should be shown.
 *
 * Notes:
 *     The callbacks linked in the arg table must return 0, 1, or 2 to
 *     indicate how many arguments were successfully parsed. For
 *     argument taking opts where a --long=value is possible, although
 *     it is technically 1 argument, still return 2, as this case is
 *     accounted for.
 *
 *     For a usage example, refer to the test program tclarg.c
 */

#ifndef BRADENLIB_CLARG_H
#define BRADENLIB_CLARG_H 1000000L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

#define CLARG_MAX(a, b) \
    ( ((a) > (b)) ? (a) : (b) )

#define clarg_parse_args_simple(argtab, argv) \
    clarg_parse_args((argtab), sizeof (argtab) / sizeof *(argtab), (argv))

typedef int (*clarg_callback_fn)(char const *param);

struct clarg {
    char              shortopt;
    char *            longopt;
    int               has_param;
    clarg_callback_fn callback;
};

static int  clarg_callback_dummy_0  (char const *unused);
static int  clarg_callback_dummy_1  (char const *unused);

static INLINE void           clarg_set_callback_normal        (clarg_callback_fn callback);
static INLINE void           clarg_set_callback_stdin         (clarg_callback_fn callback);
static INLINE void           clarg_set_callback_unrecognized  (clarg_callback_fn callback);
static INLINE void           clarg_set_callback_invalid       (clarg_callback_fn callback);
static INLINE struct clarg * clarg_find_short                 (struct clarg *argtab, size_t argtablen, char key);
static INLINE struct clarg * clarg_find_long                  (struct clarg *argtab, size_t argtablen, char *key);
static INLINE int            clarg_parse_longopt              (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
static INLINE int            clarg_parse_multiopt             (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
static INLINE int            clarg_parse_shortopt             (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
static INLINE int            clarg_parse_arg                  (struct clarg *argtab, size_t argtablen, char *arg, char *nextarg);
static INLINE int            clarg_parse_args                 (struct clarg *argtab, size_t argtablen, char **argv);

static clarg_callback_fn clarg_callback_normal       = clarg_callback_dummy_1;
static clarg_callback_fn clarg_callback_stdin        = clarg_callback_dummy_1;
static clarg_callback_fn clarg_callback_unrecognized = clarg_callback_dummy_0;
static clarg_callback_fn clarg_callback_invalid      = clarg_callback_dummy_0;
static int clarg_disable_opt_parsing;

static int
clarg_callback_dummy_0(char const *unused)
{
    (void)unused;
    return 0;
}

static int
clarg_callback_dummy_1(char const *unused)
{
    (void)unused;
    return 1;
}

static INLINE void
clarg_set_callback_normal(clarg_callback_fn callback)
{
    clarg_callback_normal = callback;
}

static INLINE void
clarg_set_callback_stdin(clarg_callback_fn callback)
{
    clarg_callback_stdin = callback;
}

static INLINE void
clarg_set_callback_unrecognized(clarg_callback_fn callback)
{
    clarg_callback_unrecognized = callback;
}

static INLINE void
clarg_set_callback_invalid(clarg_callback_fn callback)
{
    clarg_callback_invalid = callback;
}

static INLINE struct clarg *
clarg_find_short(struct clarg *argtab, size_t argtablen, char key)
{
    size_t i;

    for (i = 0; i < argtablen; ++i)
        if (argtab[i].shortopt == key)
            return argtab + i;

    return NULL;
}

static INLINE struct clarg *
clarg_find_long(struct clarg *argtab, size_t argtablen, char *key)
{
    size_t i;
    size_t keylen = strcspn(key, "=");
    size_t optlen;

    for (i = 0; i < argtablen; ++i) {
        if (argtab[i].longopt == NULL)
            continue;

        optlen = strlen(argtab[i].longopt);

        if (optlen == keylen && memcmp(argtab[i].longopt, key, optlen) == 0)
            return argtab + i;
    }

    return NULL;
}

/* 1. if the entry has a param and no param is detected, or the entry
 *    does not have a param but a param is detected, the invocation is
 *    invalid. (A AND NOT B) OR (NOT A AND B) can also be expressed as
 *    A XOR B
 *
 * 2. `--` disables further opt parsing. Opts after this arg will be
 *    treated literally.
 *
 *    Example:
 *
 *        `mv file1 file2 -- -i`
 *
 *    moves `file1` and `file2` to a directory named `-i`, whereas
 *
 *        `mv file1 file2 -i`
 *
 *    attempts to rename `file1` to `file2` and interactively prompts
 *    the user if there is a naming collision.
 *
 * 3. This is a special case. Since --longopt=value is actually one arg,
 *    we need to indicate to clarg_parse_args to advance by one argument
 *    instead of two. The client code is still expected to return 2,
 *    however.
 */
static INLINE int
clarg_parse_longopt(struct clarg *argtab, size_t argtablen, char *arg, char *nextarg)
{
    struct clarg *match;
    char *argp = arg;
    size_t arglen_full = strlen(argp);
    size_t arglen = strcspn(arg, "=");
    int detect_param = arglen_full != arglen;
    int res;

    if (arglen_full == 2) { /* 2 */
        clarg_disable_opt_parsing = 1;
        return 1;
    }

    argp += 2;
    arglen -= 2;
    arglen_full -= 2;
    match = clarg_find_long(argtab, argtablen, argp);

    if (match == NULL)
        return clarg_callback_unrecognized(arg);

    if (match->has_param ^ detect_param) /* 1 */
        return clarg_callback_invalid(arg);

    if (match->has_param) {
        res = match->callback(argp + arglen + 1);

        if (res == 2)
            return 1; /* 3 */
    }

    return match->callback(nextarg);
}

/* 1. Only one opt in a multiopt may take a parameter.
 *
 *    Example:
 *
 *        `tar -zcvf file.tar.gz ...`
 *
 *    is more strict, only allowing the last multiopt to receive a param.
 *
 * 2. all opts in the multiopt must be successfully parsed. Otherwise,
 *    one of them is unrecognized. Note that `n_parsed` is not guaranteed
 *    to have the same value as `i`
 *
 * 3. This is a size_t to silence a warning (compare size_t == int)
 */
static INLINE int
clarg_parse_multiopt(struct clarg *argtab, size_t argtablen, char *arg, char *nextarg)
{
    struct clarg *match;
    char *argp = arg + 1;
    size_t arglen = strlen(argp);
    size_t i;
    int n_has_param = 0;
    size_t n_parsed = 0; /* 3 */
    int callback_res;
    int retvalue = 0;

    for (i = 0; i < arglen; ++i) {
        match = clarg_find_short(argtab, argtablen, argp[i]);

        if (match == NULL)
            continue;

        if ((n_has_param += match->has_param) > 1) /* 1 */
            return clarg_callback_invalid(arg);

        callback_res = match->callback(nextarg);
        retvalue = CLARG_MAX(retvalue, callback_res);
        ++n_parsed;
    }

    if (n_parsed == arglen) /* 2 */
        return retvalue;

    return clarg_callback_unrecognized(arg);
}

/* 1. `-` conventionally means "use stdin as the file". It triggers
 *    `clarg_callback_stdin`
 *
 *    Example:
 *
 *        `echo hello | vim -`
 *
 *    will make vim read from stdin before it goes into interactive mode.
 *
 *        `gcc -xc -`
 *
 *    reads from stdin and interprets it as C code.
 *
 * 2. Arguments like `-abc` are multiopts. Defer to
 *    `clarg_parse_multiopt`
 */
static INLINE int
clarg_parse_shortopt(struct clarg *argtab, size_t argtablen, char *arg, char *nextarg)
{
    struct clarg *match;
    size_t arglen = strlen(arg);

    if (arglen == 1) /* 1 */
        return clarg_callback_stdin(arg);

    if (arglen > 2) /* 2 */
        return clarg_parse_multiopt(argtab, argtablen, arg, nextarg);

    match = clarg_find_short(argtab, argtablen, arg[1]);

    if (match == NULL)
        return clarg_callback_unrecognized(arg);

    return match->callback(nextarg);
}

static INLINE int
clarg_parse_arg(struct clarg *argtab, size_t argtablen, char *arg, char *nextarg)
{
    if (arg[0] != '-' || clarg_disable_opt_parsing)
        return clarg_callback_normal(arg);

    if (arg[1] == '-')
        return clarg_parse_longopt(argtab, argtablen, arg, nextarg);

    return clarg_parse_shortopt(argtab, argtablen, arg, nextarg);
}

static INLINE int
clarg_parse_args(struct clarg *argtab, size_t argtablen, char **argv)
{
    size_t i;
    int res;

    for (i = 1; argv[i] != NULL; ) {
        res = clarg_parse_arg(argtab, argtablen, argv[i], argv[i + 1]);

        if (res == 0)
            return 0;

        i += res;
    }

    return 1;
}

#undef CLARG_MAX

#endif /* BRADENLIB_CLARG_H */
