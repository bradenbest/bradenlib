/*
 * Name:
 *     rejectcpp v1.0.2
 *
 * Synopsis:
 *     #include "rejectcpp.h"
 *
 * Description:
 *     Rejects compilers that define the __cplusplus macro with an error
 *     message.
 *
 *     Use to dissuade people from attempting to compile a C project
 *     with a C++ compiler.
 */

#ifndef BRADENLIB_REJECTCPP_H
#define BRADENLIB_REJECTCPP_H 1000002L

#ifdef __cplusplus
#    error C++ compiler detected. Use a C compiler.
#endif

static int rejectcpp_silence_empty_translation_unit_warning;

#endif /* BRADENLIB_REJECTCPP_H */
