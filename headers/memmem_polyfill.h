/*
 * Name:
 *     memmem_polyfill v1.0.2
 *
 * Synopsis:
 *     #define MEMMEM_POLYFILL_USE_WRAPPER
 *     #include "memmem_polyfill.h"
 *
 *     void *  memmem           (void const *haystack, size_t haystacklen, void const *needle, size_t needlelen);
 *     void *  memmem_polyfill  (void const *haystack, size_t haystacklen, void const *needle, size_t needlelen);
 *
 * Arguments:
 *     haystack:    string
 *     haystacklen: length of haystack
 *     neelde:      substring
 *     needlelen:   length of needle
 *
 * Description:
 *     provides a polyfill implementation of the nonstandard memmem
 *     function function.
 *
 *     The function itself is called `memmem_polyfill`, but there is also
 *     a macro `memmem` to wrap it. Define MEMMEM_POLYFILL_USE_WRAPPER to
 *     make it available.
 *
 * Return Value:
 *     Pointer to offset in haystack where needle first occurs, or NULL
 *     if no match is found
 */
#ifndef BRADENLIB_MEMMEM_POLYFILL_H
#define BRADENLIB_MEMMEM_POLYFILL_H 1000002L

#include <string.h>

#ifdef MEMMEM_POLYFILL_USE_WRAPPER
#ifndef memmem
#    define memmem(haystack, haystacklen, needle, needlelen) \
        memmem_polyfill( (haystack), (haystacklen), (needle), (needlelen) )
#else
#    warn "pre-existing memmem macro found."
#endif /* memmem */
#endif /* MEMMEM_POLYFILL_USE_WRAPPER */

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

static INLINE void *  memmem_polyfill  (void const *haystack, size_t haystacklen, void const *needle, size_t needlelen);

static INLINE void *
memmem_polyfill(void const *haystack, size_t haystacklen, void const *needle, size_t needlelen)
{
    char const *haystack_cp = haystack;
    size_t i;

    for (i = 0; i < haystacklen; ++i)
        if (needlelen <= (haystacklen - i) && memcmp(haystack_cp + i, needle, needlelen) == 0)
            return (void *)(haystack_cp + i);

    return NULL;
}

#endif /* BRADENLIB_MEMMEM_POLYFILL_H */
