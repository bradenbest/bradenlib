/*
 * Name:
 *     file_get_size v1.0.2
 *
 * Synopsis:
 *     size_t  file_get_size           (FILE *file);
 *     size_t  file_get_size_relative  (FILE *file);
 *     size_t  file_get_delim_length   (FILE *file, char const *delim, size_t delim_len);
 *     size_t  file_seek_to_delim      (FILE *file, char const *delim, size_t delim_len);
 *     size_t  file_seek_past_delim    (FILE *file, char const *delim, size_t delim_len);
 *
 * Arguments:
 *     file:  file to be parsed
 *     delim: string of delimeters to terminate on
 *
 * Description:
 *     file_get_size calculates the size of entire file and resets the
 *     file position to where it was before the call.
 *
 *     file_get_size_relative is the same except the size is calculated
 *     relative to the current file position rather than the whole file
 *
 *     file_get_delim_length calculated the number of bytes from the
 *     current file position to any of the delimeters specified in delim.
 *     The file is seeked back to the original position. EOF is also
 *     considered a terminator, so if the function returns 0, use feof(3)
 *     to check for end-of-file.
 *
 *     file_seek_to_delim is the same as file_get_delim_length except it
 *     doesn't reset the file position
 *
 *     file_seek_past_delim is like file_seek_to_delim but the opposite
 *     and more aggressive. Instead of seeking until the first delim
 *     character is found, it seeks until the first non-delim character,
 *     and seeks back one character.
 *
 *     None of the functions (see Exceptions) should be used on a device
 *     like stdin where seeking is not possible.
 *
 * Exceptions:
 *     file_seek_to_delim does not seek backwards, so it is currently the
 *     only function that is safe to use with a non-seekable device like
 *     stdin.
 *
 * Return Value:
 *     file_get_size returns the size in bytes of file, or 0 if there was
 *     an error seeking to the EOF.
 *
 *     file_get_size_relative returns the size in bytes of the file minus
 *     the current file position for the effective "remaining size", or 0
 *     if there was an error seeking to the EOF.
 *
 *     file_get_delim_length returns the number of bytes up to
 *     (excluding) delim from the current file position
 *
 *     file_seek_to_delim is the same as file_get_delim_length
 *
 *     file_seek_past_delim returns the number of characters that were
 *     seeked past
 *
 * See Also:
 *     get_line
 */
#ifndef BRADENLIB_FILE_GET_SIZE
#define BRADENLIB_FILE_GET_SIZE 1000002L

#include <stdio.h>
#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

static INLINE size_t  file_get_size           (FILE *file);
static INLINE size_t  file_get_size_relative  (FILE *file);
static INLINE size_t  file_get_delim_length   (FILE *file, char const *delim, size_t delim_len);
static INLINE size_t  file_seek_to_delim      (FILE *file, char const *delim, size_t delim_len);
static INLINE size_t  file_seek_past_delim    (FILE *file, char const *delim, size_t delim_len);

static INLINE size_t
file_get_size(FILE *file)
{
    long res;
    long pos = ftell(file);

    if (fseek(file, 0L, SEEK_END) != 0) {
        perror("file_get_size: error seeking to EOF");
        return 0;
    }

    res = ftell(file);

    if (fseek(file, pos, SEEK_SET) != 0)
        perror("file_get_size: error seeking back");

    return (size_t)res;
}

static INLINE size_t
file_get_size_relative(FILE *file)
{
    long res = file_get_size(file);
    long pos = ftell(file);

    if (res == 0)
        return 0;

    return (size_t)(res - pos);
}

static INLINE size_t
file_get_delim_length(FILE *file, char const *delim, size_t delim_len)
{
    size_t length;
    long pos = ftell(file);

    length = file_seek_to_delim(file, delim, delim_len);

    if (fseek(file, pos, SEEK_SET) != 0)
        perror("file_get_delim_length: error seeking back");

    return length;
}

static INLINE size_t
file_seek_to_delim(FILE *file, char const *delim, size_t delim_len)
{
    size_t length = 0;
    int ch;

    while ((ch = fgetc(file)) != EOF && memchr(delim, ch, delim_len) == NULL)
        ++length;

    return length;
}

static INLINE size_t
file_seek_past_delim(FILE *file, char const *delim, size_t delim_len)
{
    int ch;
    size_t length = 0;

    printf("length: %lu\n", length);
    while ((ch = fgetc(file)) != EOF && memchr(delim, ch, delim_len) != NULL)
        ++length;

    printf("length: %lu\n", length);
    printf("EOF: %u\n", feof(file));
    printf("last char: %02x ('%c')\n", ch, ch);

    if (!feof(file) && fseek(file, -1, SEEK_CUR) != 0)
        perror("file_seek_past_delim: error seeking back");

    return length;
}

#endif /* BRADENLIB_FILE_GET_SIZE */
