/*
 * Name:
 *     array_remove v1.0.2
 *
 * Synopsis:
 *     void  array_remove        (void *base, size_t nmemb, size_t size, size_t index);
 *     void  array_removez       (void *base, size_t nmemb, size_t size, size_t index);
 *     void  array_remove_fast   (void *base, size_t nmemb, size_t size, size_t index);
 *     void  array_removez_fast  (void *base, size_t nmemb, size_t size, size_t index);
 *
 * Arguments:
 *     base:  base pointer to array
 *     nmemb: number of elements
 *     size:  size of each element
 *     index: index of element to remove
 *
 * Flags:
 *     ARRAY_REMOVE_FLAG_ZEROFILL: zero-fill last element after remove
 *     currently ignored, but preserved in case of future flags
 *
 * Description:
 *     Removes an element from an array. The element at [length - 1]
 *     becomes junk.
 *
 *     `array_remove` preserves the order of the elements by shifting
 *     each element past `index` over, and runs in O(n) time.
 *
 *     `array_remove_fast` works by overwriting the element at `index`
 *     with the final element, running in O(1) time.
 *
 *     Both functions leave a copy of the final element at the end, with
 *     the expectation that it will be overwritten at a later point. The
 *     `z` variants explicitly overwrite the final element with zeros.
 *
 *     In all cases, if nmemb or size are 0, the behavior is undefined
 */
#ifndef BRADENLIB_ARRAY_REMOVE_H
#define BRADENLIB_ARRAY_REMOVE_H 1000002L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

enum array_remove_flags {
    ARRAY_REMOVE_FLAG_NONE     = 0x0,
    ARRAY_REMOVE_FLAG_ZEROFILL = 0x1,
    ARRAY_REMOVE_FLAG_END
};

static INLINE void  array_remove_implementation       (void *base, size_t nmemb, size_t size, size_t index, int flags);
static INLINE void  array_remove_fast_implementation  (void *base, size_t nmemb, size_t size, size_t index, int flags);

static INLINE void  array_remove        (void *base, size_t nmemb, size_t size, size_t index);
static INLINE void  array_removez       (void *base, size_t nmemb, size_t size, size_t index);
static INLINE void  array_remove_fast   (void *base, size_t nmemb, size_t size, size_t index);
static INLINE void  array_removez_fast  (void *base, size_t nmemb, size_t size, size_t index);

static INLINE void
array_remove_implementation(void *base, size_t nmemb, size_t size, size_t index, int flags)
{
    size_t i;
    (void) flags;

    for (i = index; i < nmemb - 1; ++i)
        memcpy((char *)base + size * i, (char *)base + size * (i + 1), size);
}

/* 1. If index refers to the last element of the array, the memcpy cannot
 *    be allowed to happen (restrict pointer / aliasing violation)
 */
static INLINE void
array_remove_fast_implementation(void *base, size_t nmemb, size_t size, size_t index, int flags)
{
    (void) flags;

    if (index == nmemb - 1) /* 1 */
        return;

    memcpy((char *)base + size * index, (char *)base + size * (nmemb - 1), size);
}

static INLINE void
array_remove(void *base, size_t nmemb, size_t size, size_t index)
{
    array_remove_implementation(base, nmemb, size, index, ARRAY_REMOVE_FLAG_NONE);
}

static INLINE void
array_removez(void *base, size_t nmemb, size_t size, size_t index)
{
    array_remove_implementation(base, nmemb, size, index, ARRAY_REMOVE_FLAG_ZEROFILL);
    memset((char *)base + size * (nmemb - 1), 0, size);
}

static INLINE void
array_remove_fast(void *base, size_t nmemb, size_t size, size_t index)
{
    array_remove_fast_implementation(base, nmemb, size, index, ARRAY_REMOVE_FLAG_NONE);
}

static INLINE void
array_removez_fast(void *base, size_t nmemb, size_t size, size_t index)
{
    array_remove_fast_implementation(base, nmemb, size, index, ARRAY_REMOVE_FLAG_ZEROFILL);
    memset((char *)base + size * (nmemb - 1), 0, size);
}

#endif /* BRADENLIB_ARRAY_REMOVE_H */
