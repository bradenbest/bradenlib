/*
 * Name:
 *     ivfind v1.0.3
 *
 * Synopsis:
 *     int const *  ivfind        (int const *iv, size_t len, int value);
 *     int const *  ivfind_delim  (int const *iv, int delim, int value);
 *
 * Arguments:
 *     iv:    int vector to search
 *     len:   length of vector
 *     value: value to search for
 *     delim: delimeter to terminate on
 *
 * Description:
 *     Finds the location of the first element in iv that matches value.
 *
 *     ivfind_delim is the same, except it takes a delimeter instead of
 *     a length
 *
 *     Similar in concept to strchr(3).
 *
 * Return Value:
 *     Returns pointer to location or NULL if no match is found.
 */
#ifndef BRADENLIB_IVFIND_H
#define BRADENLIB_IVFIND_H 1000003L

#include <stddef.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

static INLINE int const *  ivfind        (int const *iv, size_t len, int value);
static INLINE int const *  ivfind_delim  (int const *iv, int delim, int value);

static INLINE int const *
ivfind(int const *iv, size_t len, int value)
{
    size_t i;

    for (i = 0; i < len; ++i)
        if (iv[i] == value)
            return iv + i;

    return NULL;
}

static INLINE int const *
ivfind_delim(int const *iv, int delim, int value)
{
    size_t i;

    for (i = 0; iv[i] != delim; ++i)
        if (iv[i] == value)
            return iv + i;

    return NULL;
}

#endif /* BRADENLIB_IVFIND_H */
