## Customizable header libraries

## Usage

You can either include the files individually in your project, or you
can use the handy makefile to roll your own header library

    $ make putsv memtoi
    cat putsv.h >> bradenlib.h
    cat memtoi.h >> bradenlib.h
    $ cp bradenlib.h my-awesome-source-tree/

## Contents (brief)

Each header has a manpage-style comment at the top describing its API

### Array / String
* array\_remove - remove an element from an array
* dynarray - dynamic arrays
* fyshuffle - fisher-yates shuffle
* ivfind - find an integer in a vector (array)
* memcnt - memory contains
* memcpyb - same as memcpy but they return additional information
* memeq - strict memory equality
* memmem\_polyfill - polyfill for nonstandard memmem function
* memspn - memory span / complementary span (see strspn)
* sorted\_insert - insert into a sorted array
* strlen\_delim - for tokenizing strings without mutating them

### I/O
* putsv - puts vector. calls puts on an array of strings

### File

* file\_get\_size
* for\_each\_line - functions for working with files with a functional programming style
* get\_line

### Utility
* benchmark - for timing code
* clarg - commandline arg parsing enabling common conventions
* flag - macros for working with bitmasks `(flags & someflag) == someflag` etc
* sassert - simple assert
* setcmpeq - macros for wrapping the `if ((var = value) == value)` pattern
* strntol - strtol but better
* tsmalloc - tracking static malloc. To assist in debugging/optimizing memory usage
* unittest - data structures and macros for simple unit testing

### Joke
* rejectcpp - for when you want to be spiteful
