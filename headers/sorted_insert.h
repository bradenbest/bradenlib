/*
 * Name:
 *     sorted_insert v1.0 - insert an element into a sorted array
 *
 * Synopsis:
 *     void  sorted_insert  (void *base, size_t nmemb, size_t size, void *elem, int (*compar)(void const *va, void const *vb));
 *
 * Arguments:
 *     base:   base address of array
 *     nmemb:  number of members (length)
 *     size:   size of a member
 *     elem:   element to be inserted
 *     compar: comparison function
 *
 * Description:
 *     `sorted_insert`
 *     inserts an element into a sorted array preserving item order.
 *     Note that the array is expected to be sorted. Also note that, as
 *     in qsort, the types of base, elem, and the two arguments to
 *     compar must all be the same type, i.e. at the same level of
 *     indirection.  if you are sorting an array of strings (base is a
 *     char **), then elem must also be a char **, and compar will
 *     receive two char **s.
 *
 * Notes:
 *     If you need a "sorted_remove" function, see header
 *     `array_remove.h`. Unlike `array_remove_fast`, `array_remove`
 *     preserves the order of the elements.
 */

#ifndef BRADENLIB_SORTED_INSERT_H
#define BRADENLIB_SORTED_INSERT_H 1000000L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

static INLINE void  sorted_insert  (void *base, size_t nmemb, size_t size, void *elem, int (*compar)(void const *va, void const *vb));

static INLINE void
sorted_insert(void *base, size_t nmemb, size_t size, void *elem, int (*compar)(void const *va, void const *vb))
{
    size_t i;

    if (nmemb == 0)
        goto exit_insert_at_start;

    for (i = nmemb - 1; i >= 0; --i) {
        if (compar(base + i * size, elem) <= 0) {
            memcpy(base + (i + 1) * size, elem, size);
            return;
        }

        memcpy(base + (i + 1) * size, base + i * size, size);
    }

exit_insert_at_start:
    memcpy(base, elem, size);
}

#endif /* BRADENLIB_SORTED_INSERT_H */
