/*
 * Name:
 *     memcpyb v1.0.4
 *
 * Synopsis:
 *     size_t  strcpyb  (char * restrict dest, char const * restrict src);
 *     void *  strcpye  (char * restrict dest, char const * restrict src);
 *     size_t  memcpyb  (void * restrict dest, void const * restrict src, size_t n);
 *     char *  memcpye  (void * restrict dest, void const * restrict src, size_t n);
 *
 * Arguments:
 *     dest: destination string to copy to
 *     src:  source string to copy from
 *     n:    number of bytes to copy
 *
 * Description:
 *     `memcpyb` and `memcpye` write n bytes of src to dest.
 *     Their behavior is the same as memcpy with one exception (see
 *     return value)
 *
 *     `strcpyb` and `strcpye` call their respective functions with
 *     strlen(src) as the third argument.
 *
 *     As in memcpy, dest and src MUST NOT overlap. If they do, the
 *     behavior is undefined as per restrict semantics.
 *
 * Return Value:
 *     The b stands for bytes. The e stands for end. These suffixes refer
 *     to the return value
 *
 *     The b functions return the number of bytes copied
 *
 *     The e functions return a pointer to the byte after the last byte
 *     copied, so that calls can be nested or chained.
 *
 * Example:
 *     See tmemcpyb.c
 */
#ifndef BRADENLIB_MEMCPYB_H
#define BRADENLIB_MEMCPYB_H 1000004L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

static INLINE size_t  strcpyb  (char * RESTRICT dest, char const * RESTRICT src);
static INLINE void *  strcpye  (char * RESTRICT dest, char const * RESTRICT src);
static INLINE size_t  memcpyb  (void * RESTRICT dest, void const * RESTRICT src, size_t n);
static INLINE void *  memcpye  (void * RESTRICT dest, void const * RESTRICT src, size_t n);

static INLINE size_t
strcpyb(char * RESTRICT dest, char const * RESTRICT src)
{
    return memcpyb(dest, src, strlen(src));
}

static INLINE void *
strcpye(char * RESTRICT dest, char const * RESTRICT src)
{
    return memcpye(dest, src, strlen(src));
}

static INLINE size_t
memcpyb(void * RESTRICT dest, void const * RESTRICT src, size_t n)
{
    memcpy(dest, src, n);
    return n;
}

static INLINE void *
memcpye(void * RESTRICT dest, void const * RESTRICT src, size_t n)
{
    memcpy(dest, src, n);
    return (char *)dest + n;
}

#endif /* BRADENLIB_MEMCPYB_H */
