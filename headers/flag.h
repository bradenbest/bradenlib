/*
 * Name:
 *     flag v1.0.1
 *
 * Synopsis:
 *     #define  FLAG_SET         (flags, mask)
 *     #define  FLAG_UNSET       (flags, mask)
 *     #define  FLAG_TOGGLE      (flags, mask)
 *     #define  FLAG_CHECK_ALL   (flags, mask)
 *     #define  FLAG_CHECK_SOME  (flags, mask)
 *
 * Arguments:
 *     flags: number holding flags
 *     mask:  number specifying one or more OR'd flags
 *
 * Description:
 *     Macros for manipulating bitmasks. These are fairly idiomatic but
 *     there's no reason -not- to include them.
 *
 *     `FLAG_SET` sets one or more flags specified in the bitmask
 *
 *     `FLAG_UNSET` removes one or more flags
 *
 *     `FLAG_TOGGLE` toggles one or more flags
 *
 *     `FLAG_CHECK_ALL` checks if all of the flags in the mask are set.
 *     For example, FLAG_CHECK_ALL(flags, 2 | 8) will yield nonzero if
 *     flags is 11 (0b1011), but not if flags is 12 (0b1100) or 3
 *     (0b0011).
 *
 *     `FLAG_CHECK_SOME` checks if at least one of the flags in the mask
 *     are set. For example, FLAG_CHECK_SOME(flags, 2 | 8) will yield
 *     nonzero for 11, 12, and 3.
 *
 * Return Value
 *     The value of the expression. `FLAG_CHECK_*` will return 0 or 1.
 *     The other macros are assignment expressions, so they will yield
 *     the new value of `flags`
 */
#ifndef BRADENLIB_FLAG_H
#define BRADENLIB_FLAG_H 1000001L

#define FLAG_SET(flags, mask) \
    ( (flags) |= (mask) )

#define FLAG_UNSET(flags, mask) \
    ( (flags) &= ~(mask) )

#define FLAG_TOGGLE(flags, mask) \
    ( (flags) ^= (mask) )

#define FLAG_CHECK_ALL(flags, mask) \
    ( (flags) & (mask) == (mask) )

#define FLAG_CHECK_SOME(flags, mask) \
    ( (flags) & (mask) > 0 )

static int flag_silence_empty_translation_unit_warning;

#endif /* BRADENLIB_FLAG_H */
