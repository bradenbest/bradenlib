/* Name:
 *     memcnt v1.0 - memory contains
 *
 * Synopsis:
 *     int  memcnt      (void const *base, size_t len, char const *set);
 *     int  memcntonly  (void const *base, size_t len, char const *set);
 *
 * Arguments:
 *     base: base pointer to string
 *     len:  length of string
 *     set:  set of characters to check against
 *
 * Macros:
 *     MEMCNT_SET_LOWERCASE: a-z
 *     MEMCNT_SET_UPPERCASE: A-Z
 *     MEMCNT_SET_DIGIT:     0-9
 *     MEMCNT_SET_ALPHA:     A-Za-z
 *     MEMCNT_SET_ALNUM:     A-Za-z0-9
 *
 * Description:
 *     `memcnt` checks that at least one character from base appears in
 *     set
 *
 *     `memcntonly` checks that all of the characters in base appear in
 *     set and nothing else. It's the inverse of `memcnt`.
 *
 * Return Value:
 *     `memcnt`, `memcntonly` return 1 for success or 0 for failure
 */

#ifndef BRADENLIB_MEMCNT_H
#define BRADENLIB_MEMCNT_H 1000000L

#include <string.h>

#ifndef INLINE
#    define INLINE
#endif

#ifndef RESTRICT
#    define RESTRICT
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#        undef RESTRICT
#        define RESTRICT restrict
#    endif
#endif

#define MEMCNT_SET_LOWERCASE "abcdefghijklmnopqrstuvwxyz"
#define MEMCNT_SET_UPPERCASE "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
#define MEMCNT_SET_DIGIT     "0123456789"
#define MEMCNT_SET_ALPHA      MEMCNT_SET_LOWERCASE  MEMCNT_SET_UPPERCASE
#define MEMCNT_SET_ALNUM      MEMCNT_SET_ALPHA  MEMCNT_SET_DIGIT

static INLINE int  memcnt      (void const *base, size_t len, char const *set);
static INLINE int  memcntonly  (void const *base, size_t len, char const *set);

static INLINE int
memcnt(void const *base, size_t len, char const *set)
{
    size_t i;
    char const *base_str = base;

    for (i = 0; i < len; ++i)
        if (strchr(set, base_str[i]) != NULL)
            return 1;

    return 0;
}

static INLINE int
memcntonly(void const *base, size_t len, char const *set)
{
    size_t i;
    char const *base_str = base;

    for (i = 0; i < len; ++i)
        if (strchr(set, base_str[i]) == NULL)
            return 0;

    return 1;
}

#endif /* BRADENLIB_MEMCNT_H */
