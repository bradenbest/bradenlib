/*
 * Name:
 *     get_line v1.0.4
 *
 * Synopsis:
 *     size_t  get_line_implementation  (char *buffer, size_t size, FILE *file, int flags);
 *     size_t  get_line                 (char *buffer, size_t size);
 *     size_t  get_line_from            (char *buffer, size_t size, FILE *file);
 *     size_t  get_line_greedy          (char *buffer, size_t size);
 *     size_t  get_line_from_greedy     (char *buffer, size_t size, FILE *file);
 *     size_t  get_line_discard         (void);
 *
 * Arguments:
 *     buffer: buffer where input will go
 *     size:   size of buffer
 *     file:   file to read from
 *     flags:  bitmask
 *
 * Flags:
 *     GET_LINE_FLAG_DISCARD_TRAILING: if this flag is set, the function
 *     will continue to consume the line when the buffer runs out of
 *     space. Otherwise, it will exit immediately.
 *
 * Description:
 *     `get_line_implementation` gets a line of input from file
 *     excluding the newline.
 *
 *     `get_line` forces GET_LINE_FLAG_DISCARD_TRAILING and sets file to
 *     stdin.
 *
 *     `get_line_from` and `get_line_from_greedy` take a file arguemnt.
 *
 *     `get_line_greedy` and `get_line_from_greedy` turn off the
 *     GET_LINE_FLAG_DISCARD_TRAILING flag.
 *
 *     `get_line_discard` reads a line of input and discards it.
 *
 *     If buffer is NULL and bufsz is not 0, the behavior is undefined.
 *     Otherwise, the function will consume the entire line of input or
 *     immediately exit depending on GET_LINE_FLAG_DISCARD_TRAILING. See
 *     `get_line_discard`. This is useful for 'press enter to continue'
 *     prompts.
 *
 *     If the buffer runs out of space before a newline is encountered
 *     and the GET_LINE_FLAG_DISCARD_TRAILING is set, the function
 *     consumes the rest of the line.
 *
 * Return Value:
 *     Returns the number of characters written to the buffer or 0. The
 *     newline and discarded trailoff are not included in this count,
 *     because they are not written to the buffer.
 *
 *     There is no distinction between 0 bytes read and EOF. To check
 *     for EOF, use feof(file).
 */

#ifndef BRADENLIB_GET_LINE_H
#define BRADENLIB_GET_LINE_H 1000004L

#include <stdio.h>

#ifndef INLINE
#    define INLINE
#endif

#ifdef __STDC_VERSION__
#    if __STDC_VERSION__ >= 199901L
#        undef INLINE
#        define INLINE inline
#    endif
#endif

enum get_line_flags {
    GET_LINE_FLAG_NONE             = 0x0,
    GET_LINE_FLAG_DISCARD_TRAILING = 0x1,
    GET_LINE_FLAG_END
};

static INLINE size_t get_line_from            (char *buffer, size_t bufsz, FILE *file);
static INLINE size_t get_line_from_greedy     (char *buffer, size_t bufsz, FILE *file);
static INLINE size_t get_line                 (char *buffer, size_t bufsz);
static INLINE size_t get_line_greedy          (char *buffer, size_t bufsz);
static INLINE size_t get_line_discard         (void);
static INLINE size_t get_line_implementation  (char *buffer, size_t bufsz, FILE *file, int flags);

static INLINE size_t
get_line_from(char *buffer, size_t bufsz, FILE *file)
{
    return get_line_implementation(buffer, bufsz, file, GET_LINE_FLAG_DISCARD_TRAILING);
}

static INLINE size_t
get_line_from_greedy(char *buffer, size_t bufsz, FILE *file)
{
    return get_line_implementation(buffer, bufsz, file, GET_LINE_FLAG_NONE);
}

static INLINE size_t
get_line(char *buffer, size_t bufsz)
{
    return get_line_from(buffer, bufsz, stdin);
}

static INLINE size_t
get_line_greedy(char *buffer, size_t bufsz)
{
    return get_line_from_greedy(buffer, bufsz, stdin);
}

static INLINE size_t
get_line_discard(void)
{
    return get_line(NULL, 0);
}

/*
 * 1. the loop conditions are a little complicated, so I broke it up into
 *    two parts. The first part will fill the buffer while there is space
 *    unless zero characters are read or the character read is a newline
 *
 * 2. the second part checks that there is no space in the buffer AND the
 *    DISCARD_TRAILING flag is set. If both of these conditions are true,
 *    then the rest of the line is consumed.
 *
 * If refactoring this function, be sure that the behavior complies with
 * the description at the top of the file. Specifically...
 *
 *  - greedy mode (DISCARD_TRAILING flag not set) MUST read everything
 *    (except newline)
 *  - normal mode MUST discard the trailoff
 *  - discard mode MUST discard everything.
 *
 *  Use tget_line.c to test this function.
 *
 *  An example of a refactoring introducing a bug would be if the fread
 *  is moved into the while header. Doing this would always read a
 *  character, which would result in a character getting lost in greedy
 *  mode. (size 10 input "Hello World!\n" -> {"Hello Worl", 10}, {"!", 1})
 */
static INLINE size_t
get_line_implementation(char *buffer, size_t bufsz, FILE *file, int flags)
{
    static char ch;
    size_t nread;
    size_t buflen = 0;

    if (file == NULL || feof(file))
        goto exit_eof;

    if (ferror(file))
        goto exit_error;

    while (buflen < bufsz) { /* 1 */
        if ((nread = fread(&ch, sizeof ch, 1, file)) == 0 || ch == '\n')
            break;

        buffer[buflen++] = ch;
    }

    if (buflen >= bufsz && (flags & GET_LINE_FLAG_DISCARD_TRAILING)) /* 2 */
        while ((nread = fread(&ch, sizeof ch, 1, file)) > 0 && ch != '\n')
            ;

    return buflen;

exit_error:
    {
        printf("get_line_implementation: Encountered I/O error.\n");
        return 0;
    }

exit_eof:
    return 0;
}

#endif /* BRADENLIB_GET_LINE_H */
