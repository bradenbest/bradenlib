/*
 * Name:
 *     tgmalloc v1.0 - tracking global malloc
 *
 * Synopsis:
 *     #define TGMALLOC_ENABLE 1
 *     #include "tgmalloc.h"
 *
 *     size_t tgmalloc_total
 *     size_t tgmalloc_inuse
 *
 * Description:
 *     Wrapper for malloc / free that tracks memory usage and can be
 *     audited through the tgmalloc_total and tgmalloc_inuse variables.
 *
 *     To use: just `#define TGMALLOC_ENABLE 1` and include this file.
 *     Calls to malloc and free will be automatically converted
 *
 *     This is inherently slower than just using malloc/free, and uses
 *     more memory (sizeof (size_t) bytes per allocation), so this is
 *     intended for debugging purposes only.
 *
 *     By default, this provides a dummy implementation and lets calls to
 *     malloc/free pass through to the libc implementation. If
 *     TGMALLOC_ENABLE is defined as 1, then it wraps with the tracking
 *     added.
 *
 *     Use the header-only version (tsmalloc) for per-module tracking
 */
#ifndef BRADENLIB_TGMALLOC_H
#define BRADENLIB_TGMALLOC_H 1000000L

#include <stdlib.h>
#include <stddef.h>

#ifndef TGMALLOC_ENABLE
#    define TGMALLOC_ENABLE 0
#endif

#if TGMALLOC_ENABLE

#define malloc(size)  tgmalloc_malloc(size)
#define free(ptr)     tgmalloc_free(ptr)

extern size_t tgmalloc_total;
extern size_t tgmalloc_inuse;

void *  tgmalloc_malloc  (size_t size);
void    tgmalloc_free    (void *ptr);

#else // TGMALLOC_ENABLE

#define tgmalloc_total  0LU
#define tgmalloc_inuse  0LU
#define tgmalloc_malloc malloc
#define tgmalloc_free   free

#endif // TGMALLOC_ENABLE

#endif // BRADENLIB_TGMALLOC_H
