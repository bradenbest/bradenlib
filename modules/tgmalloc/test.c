#include <stdlib.h>
#include <stdio.h>

#include "tgmalloc.h"

void leakmemory(void);

int main()
{
    int *foo;

    printf("usage: %lu in use / %lu total\n", tgmalloc_inuse, tgmalloc_total);
    foo = malloc(sizeof *foo);
    printf("call: malloc(%lu) => %p\n", sizeof *foo, foo);
    printf("usage: %lu in use / %lu total\n", tgmalloc_inuse, tgmalloc_total);
    *foo = 34;
    printf("foo = %u\n", *foo);
    free(foo);
    printf("call: free(%p)\n", foo);
    printf("usage: %lu in use / %lu total\n", tgmalloc_inuse, tgmalloc_total);
    leakmemory();
    printf("call: leakmemory\n");
    printf("usage: %lu in use / %lu total\n", tgmalloc_inuse, tgmalloc_total);
    return 0;
}
