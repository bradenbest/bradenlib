#include "tgmalloc.h"

#if TGMALLOC_ENABLE

size_t tgmalloc_total;
size_t tgmalloc_inuse;

void *
tgmalloc_malloc(size_t size)
{
    void *res = (malloc)(sizeof tgmalloc_total + size);
    size_t *sizep = res;

    if (res == NULL)
        return NULL;

    *sizep = size;
    tgmalloc_total += size;
    tgmalloc_inuse += size;
    return res + sizeof tgmalloc_total;
}

void
tgmalloc_free(void *ptr)
{
    size_t *sizep;
    void *realptr;

    if (ptr == NULL)
        return;

    realptr = ptr - sizeof tgmalloc_total;
    sizep = realptr;
    tgmalloc_inuse -= *sizep;
    (free)(realptr);
}

#endif // TGMALLOC_ENABLE
