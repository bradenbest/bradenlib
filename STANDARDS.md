## Headers

Headers must be in a particular format.

The first part is a manpage-like header comment

```c
/*
 * Section:
 *     contents
 *
 * Section:
 *     contents
 */
 ```

Essential sections are `Name`, `Synopsis`, `Arguments`, `Description`, and `Return Value`. Though there can be other
sections like `Example`, `See Also`, or a section named after something in synopsis or arguments that expands on something

Name is the name of the module and version. Synopsis summarizes any structs, functions or macros that are part of the
interface. Arguments describes arguments to each function in more detail. Description describes what each function
*does*. Return Value describes what each function *returns*

For a hypothetical `minmax.h`, it would look something like this:

```c
/*
 * Name:
 *     minmax v1.0
 *
 * Synopsis:
 *     int  clamp (int value, int min, int max);
 *
 *     #define MIN (a, b)
 *     #define MAX (a, b)
 *
 * Arguments:
 *     value: value to clamp
 *     min:   min value
 *     max:   max value
 *     a:     first number
 *     b:     second number
 *
 * Description:
 *     `clamp` clamps value to a minimum of min and a maximum of max
 *
 *     `MIN` chooses the lesser (min) of two values
 *
 *     `MAX` chooses the greater (max) of two values
 *
 * Return Value:
 *     clamp returns the clamped value
 *
 *     MIN returns the min
 *
 *     MAX returns the max
 */
...
 ```

After this is the header guard

```c
#ifndef BRADENLIB_MINMAX_H
#define BRADENLIB_MINMAX_H 1000000L
...
#endif // BRADENLIB_MINMAX_H
```

The version number is attached to the header guard, in the format `ABBBCCCL`, where A is the major, B is the minor, and
C is the patch. So v2.1.32 would look like `2001032L`. The purpose is so that the version of a header can always be
identified at compile time. This isn't really used in the headers, but it will be used in any module libraries that use
these headers. Here's what a dependency for minmax v1.1 might look like:

```c
#if !defined(BRADENLIB_MINMAX_H) || BRADENLIB_MINMAX_H < 1001000L
#    error "[Bradenlib] somefile.c: minmax.h v1.1 required"
#endif
```

Inside the header guard goes the implementation. All functions must be `static inline` so that compilers will
appropariately inline them instead of generating a function call, and hopefully erase unused functions. Put the
functions underneath the standard library includes, types, function signatures etc.

An additional design goal of the header part of this library is that the header-only libraries must not depend on each
other. This is to keep it simple and easy to use, so that you can just e.g. `make strntol fyshuffle` to make a header
`bradenlib.h` that has those two components. A user should not get a compile error because they're missing a header
unless they are using a module (see below)

As for other comments, limit them. If you must comment on something inside a function, use a headnote.

```c
    ...
    if (feof(f)) // 1
        return 0;
    ...
```

Then at the top of the function, write the note which the `// 1` is referring to.

```c
/* 1. we must call feof twice to compensate for implementations of
 *    getchar that do not comply with the standard. Insert explanation
 *    here.
 * 2. Another comment explaining why XYZ was done.
 */
static inline void
some_function(...)
{
    ...
```

Take note of how the text is aligned to justify after the `1.` for ease of readability. Hoisting comments out of the
code like this makes it a lot easier to read. It's just much cleaner IMO.

## Modules

I haven't decided on this yet, but I have some ideas:

1. design the modules as individual C files with their own headers, makefile generates .o files and copy the object and header over
2. makefile puts multiple modules into a single .a file and pastes their headers into a single header. 
   I haven't worked out how this would flow into source code distribution. You can't exactly include an ar archive or .o object in a source tree.
3. Include dependency lists in both .c and .h file
4. put each module in their own directory with their own makefile that will paste the depended upon headers and the
   module's `module.def.h` header into a single header `module.h` and tell the user they're good to go with copy/pasting
   the files to their source tree.

I like approach #4 the best.

Since modules will be compiled separately, it won't make sense to use `static inline`. They should have a similar header
format to the header libraries other than that, though.
