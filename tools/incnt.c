#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "xilist.c"
#include "xfilequeue.c"
#include "xargs.c"
#include "xutil.c"

struct options {
    int tabvalue;
    int quiet;
    int indentcap;
};

struct filestat {
    FILE *fp;
    int counts[20];
    int lineno;
    int maxlineno;
    int maxindent;
};

static int arg_default (char const *arg);
static int arg_dash    (char const *arg);
static int arg_h       (char const *arg);
static int arg_t       (char const *arg);
static int arg_q       (char const *arg);
static int arg_c       (char const *arg);

static void report_linelist_callback (int value, int iterno);

static int  parse_line       (struct filestat *fs);
static void report_linelist  (void);
static void report_counts    (struct filestat *fs);
static void parse_file       (char const *filename, FILE *fp);
static void usage            (void);

static struct options options = {
    .tabvalue = 4,
    .indentcap = 4,
};

static struct argmap incnt_args[] = {
    {255,  arg_default},
    {'\0', arg_dash},
    {'h',  arg_h},
    {'t',  arg_t},
    {'q',  arg_q},
    {'c',  arg_c},
    {.end = 1}
};

static int
arg_default(char const *arg)
{
    filequeue_add(arg, fopen(arg, "r"));
    return 1;
}

static int
arg_dash(char const *arg)
{
    filequeue_add("stdin", stdin);
    return 1;
}

static int
arg_h(char const *arg)
{
    usage();
    return 1;
}

static int
arg_t(char const *arg)
{
    options.tabvalue = atoi(arg);
    return 2;
}

static int
arg_q(char const *arg)
{
    options.quiet = 1;
    return 1;
}

static int
arg_c(char const *arg)
{
    options.indentcap = clamp(atoi(arg), 0, 19);
    return 2;
}

static void
report_linelist_callback(int value, int iterno)
{
    printf("%s%-3u", (iterno % 10 == 0) ? ("\n    ") : (" "), value);
}

static int
parse_line(struct filestat *fs)
{
    int ch;
    int spacecnt = 0;
    int tabcnt = 0;
    int ignore = 0;
    int indents;

    fs->lineno += 1;

    while((ch = fgetc(fs->fp)) != EOF && ch != '\n'){
        if(ignore)
            continue;

        if(ch == '\t')
            ++tabcnt;
        else if(ch == ' ')
            ++spacecnt;
        else
            ignore = 1;
    }

    if(ch == EOF)
        return 0;

    indents = (spacecnt / options.tabvalue) + tabcnt;
    fs->counts[clamp(indents, 0, options.indentcap)] += 1;

    if(indents == fs->maxindent)
        ilist_push(fs->lineno) || ilist_clear();

    if(indents > fs->maxindent){
        fs->maxindent = indents;
        fs->maxlineno = fs->lineno;
        ilist_clear();
        ilist_push(fs->lineno) || ilist_clear();
    }

    return 1;
}

static void
report_linelist(void)
{
    if(ilist_isempty())
        return;

    printf("List of lines with this indent level:");
    ilist_foreach(report_linelist_callback);
    puts("");
}

static void
report_counts(struct filestat *fs)
{
    printf("Lines per indent level:\n");

    for(int i = 0; i <= options.indentcap; ++i)
        if(fs->counts[i])
            printf("    level %u%s: %u\n", i, i == options.indentcap ? "+" : "", fs->counts[i]);

    puts("");
}

static void
parse_file(char const *filename, FILE *fp)
{
    struct filestat fs = {fp};

    if(fp == NULL){
        printf("Can't open file '%s'\n", filename);
        goto CLEANUP;
    }

    while(parse_line(&fs))
        ;

    if(options.quiet){
        printf("%-2u %s\n", fs.maxindent, filename);
        goto CLEANUP;
    }

    printf("File %s:\n", filename);
    printf("Biggest indent level: %u\n", fs.maxindent);
    report_linelist();
    report_counts(&fs);

CLEANUP:
    ilist_clear();
    fclose_safe(fp);
}

static void
usage(void)
{
    static char const *lines[] = {
        "incnt - count indents",
        "usage: icnt [options] <files>",
        "options:",
        "  -         add stdin to file list",
        "  --        interpret everything after this literally",
        "  -h        show this help",
        "  -t VALUE  set how many spaces are an indent (default = 4)",
        "  -c VALUE  set indent level cap (default = 4)",
        "  -q        set quiet mode (less verbose output)",
        NULL
    };

    putsv(lines);
    exit(1);
}

int
main(int argc, char const **argv)
{
    if(argc < 2)
        usage();

    args_parseall(incnt_args, argv + 1);
    filequeue_foreach(parse_file);
    filequeue_clear();

    return 0;
}
