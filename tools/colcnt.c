#include <stdio.h>

#define MIN(a, b) \
    ( ((a) < (b)) ? (a) : (b) )

#define MAX_LINELEN (120)

#define MAXBUFSZ (4096)

struct file {
    char *name;
    FILE *fp;
};

struct {
    int maxlinelen; // TODO: args
} options = {120};

int
parse_line(struct file *f)
{
    static char buf[MAXBUFSZ];
    char *bufp = buf;
    int ch;
    int colcnt = 0;
    int width;

    while((ch = fgetc(f->fp)) != EOF && ch != '\n'){
        ++colcnt;

        if(bufp - buf < MAX_LINELEN)
            *bufp++ = ch;
    }

    width = MIN(bufp - buf, MAX_LINELEN);
    printf("%-3u  %s  %.*s%s\n", colcnt, f->name, width, buf, width == MAX_LINELEN ? "..." : "");

    if(ch == EOF)
        return 0;

    return 1;
}

int
parse_file(char **args)
{
    struct file f;

    if(*args == NULL)
        return 0;

    f.name = *args;
    f.fp = fopen(f.name, "r");

    if(f.fp == NULL)
        return 0;

    while(parse_line(&f))
        ;

    fclose(f.fp);
    return 1;
}

int
main(int argc, char **argv)
{
    if(argc < 2){
        puts("colcnt - count columns per line");
        puts("usage: colcnt <files> ");

        return 1;
    }

    while(parse_file(++argv))
        ;

    return 0;
}
