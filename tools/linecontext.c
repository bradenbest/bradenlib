#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MEMMEM_POLYFILL_USE_WRAPPER
#include "get_line.h"
#include "putsv.h"
#include "memmem_polyfill.h"

#define PROGNAME "linecontext"

struct line {
    char *        buffer;
    struct line * next;
};

static inline int   line_push     (char const *buffer);
static inline void  line_clear    (void);
static inline void  seek_to_line  (FILE *fp, int target_lineno);
static inline void  print_region  (void);

int  main  (int argc, char **argv);

static char const *usage_text[] = {
    PROGNAME " - show selected line and surrounding context",
    "usage: " PROGNAME " <file> <line> [before] [after]",
    "",
    "file:   the file to read. If `-`, stdin is used",
    "line:   line number to seek to",
    "before: string to match for line to stop on (default = empty line)",
    "after:  string to match for line to stop on (default = empty line)",
    "",
    "example: `" PROGNAME " 150 [tag]` will seek upward from line 150 until",
    "         a line that starts with the string `[tag]`, and downward until",
    "         an empty line",
    NULL,
};

static struct line head;
static struct line *tail;
static char const *before;
static char const *after;

static inline int
line_push(char const *buffer)
{
    struct line *res = malloc(sizeof *res);

    if (res == NULL)
        return 0;

    *res = (struct line){
        strdup(buffer),
        NULL,
    };

    if (tail == NULL)
        tail = &head;

    tail->next = res;
    tail = res;
    return 1;
}

static inline void
line_clear(void)
{
    struct line *next = NULL;

    for (struct line *selline = head.next; selline != NULL; selline = next) {
        next = selline->next;
        free(selline->buffer);
        free(selline);
    }

    tail = &head;
    tail->next = NULL;
}

static inline void
seek_to_line(FILE *fp, int target_lineno)
{
    char linebuf[4096];
    size_t len;
    int lineno = 0;

    while ((len = get_line_from(linebuf, 4095, fp)) > 0 || (!feof(fp) && !ferror(fp))) {
        ++lineno;
        linebuf[len] = '\0';

        if (before == NULL && len == 0)
            line_clear();

        if (before != NULL && memmem(linebuf, len, before, strlen(before)) == linebuf)
            line_clear();

        if (!line_push(linebuf))
            abort();

        if (lineno == target_lineno)
            break;
    }

    if (ferror(fp))
        goto exit_ferror;

    if (feof(fp))
        return;

    while ((len = get_line_from(linebuf, 4095, fp)) > 0 || (!feof(fp) && !ferror(fp))) {
        linebuf[len] = '\0';

        if (!line_push(linebuf))
            abort();

        if (after == NULL && len == 0)
            return;

        if (after != NULL && memmem(linebuf, len, after, strlen(after)) == linebuf)
            return;
    }

    if (ferror(fp))
        goto exit_ferror;

    if (feof(fp))
        return;

    return;

exit_ferror:
    fprintf(stderr, PROGNAME ": an error occurred while reading the file\n");
    return;
}

static inline void
print_region(void)
{
    for (struct line *selline = head.next; selline != NULL; selline = selline->next)
        puts(selline->buffer);
}

int
main(int argc, char **argv)
{
    char const *filename;
    FILE *fp;
    int lineno = 0;

    if (argc < 3)
        goto exit_usage;

    filename = argv[1];
    lineno = atoi(argv[2]);

    if ((fp = fopen(filename, "rb")) == NULL)
        goto exit_fopen_fail;

    if (argc >= 4)
        before = argv[3];

    if (argc >= 5)
        after = argv[4];

    seek_to_line(fp, lineno);
    print_region();
    fclose(fp);
    line_clear();
    return 0;

exit_usage:
    putsv(usage_text);
    return 1;

exit_fopen_fail:
    fprintf(stderr, PROGNAME ": failed to open file %s for reading\n", filename);
    return 2;
}
