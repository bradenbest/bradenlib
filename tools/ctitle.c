// ctitle v1.0.1
#include <stdio.h>

void
change_title(char *new_title)
{
    printf("\033]0;%s\007", new_title);
}

int
main(int argc, char **argv)
{
    if(argc > 1)
        change_title(argv[1]);
    else
        puts("usage: ctitle <new title>");

    return 0;
}
