# Bradenlib - a collection of general-purpose code

This is not meant to be distributed as linkable binaries. They are merely simple source files that can be included and
compiled into a project. Headers, modules, and tools

## Modules:

Nothing yet

## Headers:

Header-only libraries to be included and/or combined directly into source code. They are versioned and documented.

## Tools:

Nothing yet
